# https://fr.wikipedia.org/wiki/Liste_de_couleurs_(pigments)
# https://www.handprint.com/HP/WCL/palette1.html#greenyellow
# https://www.handprint.com/HP/WCL/huepurity.html
abbrev e
system ciejab

# pig 	j    	a      	b	name
# 1     2       3      	 4   	5-*
PR122	38	71	 1	magenta quina		semitransparent
PV19:rose 44	72	13	rose quina		semitransparent
PV19:violet 27  58       7      violet quina		semitransparent
PR83	33	70	22	cramoisie d'alizarine	transparent
PR108	41	78	36	cadium red 		opaque
PR108:sc 46     80      43      cadmium scarlet         opaque
PR108:deep 36   73      28      cadmium red deep        opaque
PR101:bs 37	40	27  	Sienne brulée		opaque
PR188	48      81      45      naphthol scarlet        semitransparent
PR170   38      81      30      naphthol AS red		semiopaque 
PR255   44      81      41      pyrrole scarlet         semitransparent
PR206   36      54      25      quinacridone maroon     semitransparent
PR254   38      85      38      pyrrole red             semiopaque
PR209   46      80      32      quinacridone red        transparent
PR177   33      76      24      anthraquinone red       transparent
PR179   29      60      21      perylene maroon         transparent
PR171   26      36       9      benzimidazolone maroon  semitransparent
PR149   35      68      29      perylene scarlet 	semiopaque
PR:NA   33      74      26      quinacridone pyrrolidone transparent
PR106   47      71      38      red vermillon french    very opaque
PB7r	34	12	20	Ombre naturelle		opaque
PY42    50      37      45      gold ochre              opaque
PY43	58	22	45	ocre jaune		semiopaque
PO20	60	59	58	orange cadium		semiopaque
PO62    65      50      59      benzimidazolone orange  semiopaque
PO73    50      75      57      pyrrole orange          semitransparent
PO43    53      72      52      perinone orange         semiopaque
PO48    42      52      37      quinacridone orange     transparent
PY37	71      34      68	jaune cadium foncé	semiopaque
PY153   71      29      69      jaune nickel dioxine    semitransparent
PY110   72      34      68      isoindolinone yellow    transparent
PY35	81	11	78	jaune cadium		semiopaque
PY35:lemon	89     -10	71	jaune cadium citron 	semiopaque
PY35:deep  71   34      68      jaune cadium fonce 	semiopaque
PBr24   68      24      48      chrome titanate         opaque
PY3	88     -12	72	jaune hansa clair	semitransparent
PY97	85      -2      80  	jaune hansa		semitransparent
PY151   84      -1      75      benzimidazolone yellow  semitransparent
PY154	83       1      77      jaune benzimidazolone   semitransparent
PY117   58      -1      60      azomethine copper 	semitransparent
PY129   53       4      49      azomethine copper 	semitransparent
PG8     26     -19      14      vert de Hooker          semitransparent 
PG36	39     -54      18   	vert phthalo YS		transparent
PG36:teal 39   -45     -17	turquoise cobalt blue	
PG17	35     -18      16	vert oxyde de chrome	opaque
PG7	31     -51       1 	vert phthalo BS		transparent
PG18	42     -48	 3	viridian		transparent
PG50:turquoise	56     -56     -19	cobalt teal blue	semiopaque
PG26	33     -27       0	cobalt green dark	opaque
PG50    50     -48       0	cobalt green BS         semiopaque
PG50:ys 42     -41      20      cobalt green YS         semiopaque
PB35	37     -36     -45	bleu ciel		semiopaque
PB36 	37     -36     -45	bleu ciel 		semiopaque
PB36:gs 35     -39     -34 	bleu ciel GS		semiopaque
PB15:3	27     -31     -48	bleu phtalo GS		transparent
PB15:1  24     -23     -49	bleu phtalo RS		transparent
PB27	19     -17     -36	prussian blue		semitransparent
PB28	34     -28     -60	bleu cobalt		semitransparent
PB29	21     -19     -68	bleu outremer		semitransparent
PB60    18       0     -32      bleu indanthrone        semiopaque 
PB33	46     -49     -45      manganese blue          semiopaque
PB17	35     -50     -46	phtalo cyan		semitransparent
PB16    23     -36     -27      phtalo turquoise	semitransparent
PB72    29     -20     -62      cobalt blue deep        semiopaque
PV15:rs	36	21     -37	violet outremer RS      semitransparent
PV15:bs	22      -4     -57      violet outremer BS      semitransparent
PV23	20	15     -27	violet dioxazine
PV16    31      39     -21      manganese violet        semitransparent
PV49	50      52     -26	cobalt violet	        semitransparent
PV42    35      74      16      quinacridone pink       semitransparent
PBr7:bs 37      40      27      Sienne brulee		semiopaque
PBr7:rs 55      23      42      Sienne naturelle	semiopaque
PBr7:bu 28      18      16      Ombre brulee		semiopaque
PBr7:ru 34      12      20      Ombre naturelle		semiopaque

# 4 unique colors ~= PV19 PY153 PG36 PB15:3
#UR	78     94	 34	rouge unique	#  20.14 deg
#UY	78	0	100	jaune unique    #  90    deg
#UG	78    -96 	 27	vert unique	# 164.25 deg
#UB	78    -54       -84	bleu unique	# 237.53 deg
# extra-spectrl hues: violet_hue = -66 deg & red hue = 24 deg

# extras
PW1	 95       0       0      blanc d'argent 
PW6	 95       0       0      blanc de titane
PBk6     21      -8       8	 perylene green-black
PBk9	  5       0       0      ivory black
