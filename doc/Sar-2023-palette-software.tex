% *************************************************
% header
% *************************************************
\documentclass[11pt]{article}
\usepackage{dessin}
\hypersetup{pdftitle={color-software}}
\title{Palette~: un logiciel d'analyse de palettes et de tableaux}
\author{\href{mailto:Pierre.Saramito@imag.fr}{Pierre.Saramito@imag.fr}}
\begin{document}
\maketitle

Il y a plusieurs situations dans lesquelles les logiciels 
peuvent être utiles~:
\begin{itemize}
\item[$\bullet$]
  \textbf{visualiser} une liste de pigments,
  donnés dans l'espace {\tt ciecam},
  avec {\tt gnuplot} en 2D~$(a,b)$ ou~$(j,c)$
  ou 
  avec {\tt paraview} en 3D~$(j,a,b)$.
  Pour afficher les pigments sous forme de pastille sur l'écran,
  il faut convertir leurs composantes {\tt ciecam}~$(j,a,b)$ en~$(r,g,b)$.
\item[$\bullet$]
  \textbf{analyser} une photo de peinture ou autre,
  et en extraire l'enveloppe de la zone parcourue dans l'espace {\tt ciecam}.
  Pour cela, il faut convertir
  les pixels en coordonnées~$(r,g,b)$ vers l'espace {\tt ciecam}~$(j,a,b)$.
  L'exemple de référence est donné par
  \citet{Evo-2008-harmony-fr}, Figs~1 et~46 pour la peinture de Gauguin~:
  il s'agit d'être capable de faire la même chose.
  Je pense aux émissions Palette d'Arte~: prendre des photos des tableaux
  de cette émission et regarder si je trouve la même palette qu'eux.
\item[$\bullet$]
  \textbf{étudier} l'effet d'un illuminant sur une palette ou une photo,
  comme dans \citet{Evo-2008-harmony-fr}, Figs~15, 38, 39 et~40-45.
  Cela permettrait de mieux comprendre comment
  l'illuminant réduit et harmonise une palette.
  Les couleurs d'une liste~$(j_i,a_i,b_i)$ sont \textbf{mélangées} à
  la couleur de l'illuminant~$(j_0,a_0,b_0)$
  pour obtenir une nouvelle liste de couleurs.
  Pour cela, il faut disposer d'un modèle mathématique
  de mélange de deux couleurs.
\end{itemize}
\bigskip
Il y a donc plusieurs points sur lesquel avancer~:
\begin{itemize}
\item[$\bullet$]
\textbf{convertir}~:
Avec {\tt gnuplot}, la fonction~{\tt hsv2rgb} ne convient pas~: obligé de tourner l'angle de teinte.
\citet{WesRipChe-2012-ciecam} proposent un code matlab mais qui n'est pas public.
On trouve aussi un
\href{https://stackoverflow.com/questions/58952430/rgb-xyz-and-xyz-lab-color-space-conversion-algorithm}
     {code c++}
pour {\tt rgb2lab}.
La librairie python
\href{https://pypi.org/project/ciecam02}
     {ciecam02}
propose une approximation de {\tt ciecam}~$(j,a,b)$ en~$(j,c,h)$
avec les fonctions {\tt jch2rgb} et {\tt rgb2jch}.
Il y a aussi la conversion d'une image entière en {\tt cielab}~$(L,a,b)$
avec la librairie
\href{https://stackoverflow.com/questions/13405956/convert-an-image-rgb-lab-with-python}
     {color}.
% verif avec ColorMine.org
Enfin, la librairie python
\href{https://github.com/njsmith/colorspacious}
     {colorspacious}
implémente la conversion sur le vrai {\tt ciecam}~$(j,a,b)$.
\item[$\bullet$]
\textbf{mélanger}~:
Voir si \citet{WesRipChe-2012-ciecam} proposent une formule~?
Sinon, on interpole linéairement.
\item[$\bullet$]
\textbf{mesurer}~:
Bruce McEvoy propose
\href{https://www.handprint.com/HP/WCL/palette1.html}
     {liste de pigments aquarelles}
avec les mesures {\tt ciecam},
mais, pour utiliser de nouveaux pigments, acryliques ou autres,
\citet{Col-2019-color-fr} suggère de les mesurer avec un
\href{https://www.nixsensor.com/mini-3-color-sensor}
     {capteur de couleur NIX}
(environ 100~euros).
Cette mesure permet d'étudier de vrais mélanges de pigments~:
comment des mélanges pondérés de deux pigments complémentaires
traversent réellement le cercle des couleurs,
idem pour un pigment et du blanc ou du noir, etc.
\end{itemize}
\bigskip

% =================================================
\section{Conversion et visualisation}
% =================================================

Pour installer la librairie python
\href{https://github.com/njsmith/colorspacious}
     {colorspacious}~:
\begin{verbatim}
  sudo apt install python3-pip python3.11-venv python3-full python-is-python3
  sudo apt install python3-docutils
  python -m venv ~/mypy
  source ~/mypy/bin/activate
  pip install colorspacious Pillow colormath numpy scipy
\end{verbatim}
Sa 
\href{https://colorspacious.readthedocs.io}
     {documentation}
donne un exemple que on place dans {\tt color\_tst.py}~:
\begin{verbatim}
  from math import *
  from colorspacious.conversion import cspace_convert
  rgb = [64, 128, 255]
  jch = cspace_convert(rgb, "sRGB255", "JCh")
  print(f'jch: rgb{rgb} -> jch{jch}')
  jab = [jch[0], jch[1]*cos(2*pi*jch[2]/360.), jch[1]*sin(2*pi*jch[2]/360.)]
  print(f'jch: rgb{rgb} -> jab{jab}')
\end{verbatim}
puis
\begin{verbatim}
  python color_tst.py
\end{verbatim}
Ceci permet d'ajouter des données {\tt rgb} au palettes en {\tt ciecam}~$(j,a,b)$
et de refaire les palettes avec des couleurs plus précises~:
\begin{verbatim}
  bash mk_palette.sh acrylic-evoy-v1b-roelofs-petillon.pal
\end{verbatim}
La représenation~3D de la palette dans \citet{Sar-2021-acrylique}
clarifie beaucoup de choses.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[height=0.50\textwidth]{oil-evoy-chagall-abj.jpg}
  \end{center}
  \caption{
    Visualisation 3d de la palette acrylique version~2 de \citet{Sar-2021-acrylique}.
  }
  \label{fig-acrylic-evoy-v1b-roelofs-petillon-abj}
\end{figure}
%
L'étape suivante est la visualisation en 3D
dans l'espace~$(j,a,b)$ avec {\tt paraview}.
Pour commencer, je rentre interactivement
tous les objets graphiques
qui peuvent permettre de localiser les couleurs~:
le plan~$(a,b)$ avec un contour circulaire,
la colonne des gris avec~$j$, 
puis de sauvegarder une trace de l'interaction en python.
Ensuite, je charge le fichier {\tt .gdat} et j'affiche
des sphères de couleur pour chaque pigment, avec leur label,
comme sur la Fig.~\ref{fig-acrylic-evoy-v1b-roelofs-petillon-abj}~:
\begin{verbatim}
  bash mk_palette.sh -3d acrylic-evoy-v1b-roelofs-petillon.pal
\end{verbatim}

% ======================================================
\section{Analyse d'une photo et déduction d'une palette}
% ======================================================

La première étape est d'extraire un nuage de pixels en base~{\tt rgb},
à partir d'un treillis de l'image, avec un pas régulier ou 
à partir d'un tirage aléatoire, puis de convertir ce nuage en~$(j,a,b)$.

La seconde étape est de visualiser ce nuage de point et de le comparer
à des pigments connus, pour en déduire la palette qui aurait 
pu être utilisée par le peintre,
ou, si c'est une photo, la palette à utiliser.

Plus sophistiqué,
on pourra essayer d'automatiser cette déduction.
Il s'agit de calculer et visualiser l'\textbf{enveloppe convexe} du nuage de points
et la superposer avec une liste de pigments dans l'espace {\tt ciecam}.
Il est alors possible d'en déduire automatiquement la palette minimale qui permet de peindre~:
(i) on élimine tous les pigments hors de l'enveloppe convexe du nuage et qui ne font pas 
    plonger l'enveloppe convexe des pigments à l'intérieur de l'enveloppe du nuage~;
(ii) ensuite, on élimine tous les pigments à l'intérieur de l'enveloppe convexe du nuage.
Faut-il calculer l'enveloppe convexe en 2D ou en 3D~?
Cela restera a tester, car le peintre a sans doute utiliser du noir et/ou du blanc.

\begin{figure}[htb]
  \begin{center}
  \begin{tabular}{c}
    \includegraphics[height=0.45\textwidth]{1897_paul-gauguin_bouquet-de-fleurs.jpg} \\
    \begin{tabular}{ccc}
    \includegraphics[height=0.29\textwidth]{1897_paul-gauguin_bouquet-de-fleurs-ab.pdf} &
    \includegraphics[height=0.29\textwidth]{1897_paul-gauguin_bouquet-de-fleurs-aj.pdf} &
    \includegraphics[height=0.29\textwidth]{1897_paul-gauguin_bouquet-de-fleurs-abj.jpg}
    \end{tabular}
  \end{tabular}
  \end{center}
  \caption{
   (haut)
   \href{https://commons.wikimedia.org/wiki/File:Paul_Gauguin_-_Bouquet_de_fleurs_-_Mus\%C3\%A9e_Marmottan-Monet.jpg}
        {Bouquet de fleurs},
     par
     \href{https://fr.wikipedia.org/wiki/Paul_Gauguin}
          {Paul Gauguin},
     1897~;
   (bas-gauche, centre)
   représentation d'un échantillonnage~$100\times 100$ de pixels dans les plans
   $(a,b)$ et~$(a,j)$.
   (bas-droite)
   et d'un échantillonnage~$10\times 10$ de pixels dans l'espace~$(a,b,j)$.
  }
  \label{fig-bouquet-fleurs}
\end{figure}
%
La librairie python
\href{https://pillow.readthedocs.io/en/stable/reference/PixelAccess.html}
     {pilow}
permet d'accéder aux pixels {\tt rgb} d'une image~:
\begin{verbatim}
  from PIL import Image
  im = Image.open ("2023-06-acrylic-landscape-drome2.jpg")
  px = im.load()
  print (px[4,4])
\end{verbatim}
Pour re-échantillonner l'image avant, voir
\href{https://pillow.readthedocs.io/en/stable/reference/Image.html}
     {resample}.
L'idée est de parcourir l'image et de sortir un {\tt .gdat}
avec les coordonnées~$(a,b,j)$ et la couleur~{\tt rgb}
puis de la visualiser avec {\tt gnuplot} ou {\tt paraview}.
Suivant \citet{Evo-2008-harmony-fr},  reprenons le tableau de Gauguin
de la Fig.~\ref{fig-bouquet-fleurs}~:
\begin{verbatim}
  bash mk_palette.sh -im 1897_paul-gauguin_bouquet-de-fleurs.jpg -sample 100
\end{verbatim}
Dans le cas~3D, paraview met du temps à afficher, et une grille~$10\times 10$ semble être le maximum~:
\begin{verbatim}
  bash mk_palette.sh -im 1897_paul-gauguin_bouquet-de-fleurs.jpg -sample 10 -3d
\end{verbatim}
%
On voit que le bleu n'est jamais utilisé pur, mais mélangé
avec son complémentaire, l'orange de la lumière du soir,
ce qui baisse son chroma et le ramène vers le centre.
Ainsi, Gauguin a utilisé ici la très classique harmonie complémentaire divisée.

% =================================================
\section{Idées en vrac}
% =================================================
\begin{itemize}
\item \textbf{image2chroma}~: finaliser man unix, install
\item \textbf{pdf2jpg.sh}~: integrer dans plot2pdf.sh avec option -raster -raster-density 2000
\item \textbf{gnuplot}~: avec les images et $500\times 500$~pixels, ça devient très lourd en pdf.
      Sortir en .png ou .jpg avec une résolution fixe.
\item \textbf{hull-fit}~: passer un .pal ou plusieurs .pig.
      Gérer une liste de fichier a utiliser pour le fit~:
      les dérouler un une liste de pigments à utiliser.
\item deux catalogues sur le même graphique~:
      \begin{verbatim}
	bash mk_palette.sh acrylic-wn3-majoros.pig acrylic-golden-heavy-body.pig -name aa
      \end{verbatim}
      Afficher le préfix abbregé devant le nom, éventuellement omis quand il n'y a qu'une collection.
      Clareté~: afficher une glyph differente : cercle/sphere, carré/cube, losange, suivant la collection.
      Dans un .pal, on peut changer cette abbrev éventuellement~:
      \begin{verbatim}
	pigment acrylic-wn3-majoros.pig       as w
	pigment acrylic-golden-heavy-body.pig as g
	pigment acrylic-derivant-matisse-structure.pig as d
	d/PO43
	w/PY35
      \end{verbatim}
      Le {\tt as} peut être omis~: on utilise alors l'abbrev du .pig.
      Dans le .pal, le préfix peut-être omis devant un pigment,
      par exemple {\tt d/PO43} ou {\tt PO43} quand il n'y a qu'une collection globale,
      pour compatibilité arrière.
      On peut aussi afficher deux palettes sur le même graphique~:
      \begin{verbatim}
	bash mk_palette.sh a.pal b.pal
      \end{verbatim}
      Idem~: afficher le préfix abbregé devant le nom, éventuellement omis quand il n'y a qu'une collection.
      Chaque palette peut utiliser une ou plusieurs catalogues.
      Si ces deux .pal utilisent deux mêmes catalogues .pig 
      mais avec des {\tt as} differentes, ça sera une erreur car on ne sait pas lequel afficher.
      Idem si les même {\tt as} pour des catalogues differents.
      Sinon, il ne faudrait pas introduire le {\tt as}, mais on risque d'avoir
      des abbrev très longues avec {\tt watercolor}, {\tt acrylic}...
      puis les marques, puis les collections, puis les bases ciecam evoy wn1 wn2 wn3...
      Il vaut mieux n'avoir qu'une lettre pour l'affichage.
\item option -average-dilution-line pour representer une image par une ligne de spheres ou box
      reliees par des lignes et correspondant a la moyenne dans une tranche de z.
      Donne aussi l'amplitude de la variation de la teinte et la teinte lorsque le chroma est maximal.
\item \textbf{visu nuance, intensité}~: représenter toute une collection .pig
      suivant ces indicateurs pour créer de nouveaux types de cousins et chercher de nouvelles harmonies.
      Pour la nuance~: graphe 2d avec~$c=|(a,b)|$ en abscisse et~$j$ en ordonnée~:
      on replie tous les angles~$h$ de la teinte.
      Pour l'intensité~: graphe 2d polaire avec la teinte~$h={\tt atan2}(b,a)$ en angle
      et~$\ell=c\times j$ en rayon.
\item \textbf{visu chroma ou nuance d'une image}~: représenter une photo d'un dessins en noir-et-blanc
      avec sont niveau de chroma rendu en niveau de gris.
      Idem pour la nuance.
\item \textbf{jch2rgb.py}~: probleme de precision, avec lepagito, on doit utiliser html/lch, qui n'est pas standard.
	Mais {\tt gimp} convertis tres bien~: regarder comment ils font~!
\item \textbf{convert.py}~: regrouper tous les {\tt lch2jab.py} de maniere systematique
      pour eviter la duplication de code.
      La combinatoire des fonctions xxx2yyy explose quadratiquement avec le nb de codage de couleur xxx, yyy.
      L'idée de {\tt colorspacious} est d'utiliser un 
      \href{graphe acyclique}
           {https://colorspacious.readthedocs.io/en/latest/reference.html\#specifying-colorspaces}
      et de calculer les chemins dedans.
      On peut le parcourir une fois pour toute et générer les chemins:
\begin{verbatim}
  paths = {
           'jch2rgb': ['jch2jab', 'jab2lab', 'lab2xyz', 'xyz2rgb'], ...
          }
\end{verbatim}
     Ensuite, reste à coder les transformations qui sont arrêtes du graphe.
\item \textbf{image2gdat.py}~: utiliser {\tt cspace\_convert} avec numpy pour etre plus rapide
\item \textbf{visu 3d de gros .pal ou .pig avec .vtk (2e idee)}~: 
      boites cubiques, on peut les tourner de l'angle hue, ou en faire des losanges~3d.
      Le principe est de faciliter le cousinage et de faire des glyphes differentes
      si fichiers .pal ou .pig differents.
\item \textbf{.pig}~: tester dans {\tt piz2pig.sh} 
      si un pigment est defini deux fois dans un .pig : erreur.
      On ne s'en appercoit qu'avec {\tt mk\_palette.sh} quand on lance la visu.
\item \textbf{2d}~: corriger les annotations~:
      violet va a la place de magenta, magenta a celle de pourpre et pourpre disparait.
\item \textbf{2d}~: gnuplot peut jouer aussi sur la transparence.
      Par exemple, la visu~2d~$aj$ de la photo de la palette~1974 de chagall est trop massive. 
\item \textbf{2d aj,bj}~: quand le cercle de pigment est derriere la colonne, il devrait etre en partie cache.
      il faut les afficher en premiers, puis affiche le cylindre,
      et enfin on fait une~3e passe pour ceux en avant
      Choisir tjs la largeur de la colonne un peu plus petite que celle des cercles, pour que ca depasse tjs un peu.
\item \textbf{3d}~: les labels rentrent dans la sphere, par exemple PW6,
      quand on est près de l'axe gris
\item \textbf{fichier .pig}~:
      permet de numériser de nouveaux matériaux~:
      aquarelle.pig acrylic.pig pastel.pig etc.
      L'idée est de numériser d'autres familles de pigments que les aquarelles de McEvoy~:
      la solution la plus simple est de convertir le rgb des photos du nuancier.
      Plus sophistiqué \citet{Col-2019-color-fr} utilise
      \href{https://www.nixsensor.com}
           {capteur de couleur NIX} 
      à~110~euros~: il ne semble pas s'agit d'un spectromètre
      mais la même société NIX vend des spectromètres à~400e et 930e.
      Enfin, Evoy a mesuré ses~$jab$ avec un
      \href{https://www.handprint.com/HP/WCL/pigmt8.html#hueangle}
           {spectromètre}
       "GretagMacbeth Spectrolino" et un logiciel associé,
      mais ce modèle n'est plus vendu.
\item \textbf{nuancier}~: numériser les coords rgb puis jab des pigments du nuancier et étudier l'évolution des mélanges~:
      notion de chemin continu d'un pigment vers un autre~: un peu comme les cousins.
      Par contre, les échantillons ne sont pas dans un .ciecam standard~:
      créer un "nuancier.ciecam" avec toutes les valeurs en vrac puis "nuancier.colpath", comme pour les cousin.
      Je pourrai ensuite numériser un vrai nuancier de pigments cousins
      et voir si on suit une ligne droite dans l'espace~$abj$.
\item \textbf{make install}~:
      renommer mk\_palette.sh en palette et faire un make install
      dans DESTDIR/bin avec tous les fichiers
      aux dans PKGDATADIR=DESTDIR/share/palette/*.py
\item \textbf{python}~: grouper au max en python. 
      Peut-on lancer paraview avec pvpython ou python ou pvbatch et ouvrir la fenetre et recupérer argv~?
      Écrire la conversion gnuplot en python dans {\tt python\_gnuplot.py}.
      Plus intégré, regarder si matplotlib fait aussi bien que gnuplot en 2d~?
\item \textbf{enveloppe convexe} d'un nuage de point~: voir 
      \href{http://www.qhull.org}
           {qhull}.
      En 3d, on pourrait calculer l'enveloppe convexe d'un grand nuage de pixel
      puis l'afficher, un peu transparente, en la superposant à une palette.
\item \textbf{fichier .pig et .pal}~: 
      plusieurs .pig dans la même palette~: si on mélange acry et oil
      ou bien plusieurs marques d'acryl par ex.
      Ca peut servir aussi si on veut tester si un pigment d'une collection extérieure
      agrandit l'enveloppe convexe globale d'une autre collection complète,
      ou au contraire, si il tombe à l'intérieur~: on visualise un .pig complet
      plus un .pal avec juste un pigment d'un autre .pig.
      Il risque d'y avoir des conflits de même id de pigments, genre {\tt PY35},
      entre deux collections~:
      on peut lever ces conflits en précisant dans le .pal la collection~:
      {\tt PY35/wn} ou {\tt PY35/cobra}.
      On introduit la notion de domaine de nom "namespace" ou de "collection", "package".
      Dans l'entête du fichier .pig~: \\
      \ \ \ \ {\tt collection wn winsor newton}\\
      Le premier arg "{\tt wn}" est l'identificateur de la collec, 
      et la suite de la ligne, une description longue.
      Sur la visu, on peut avoir des symboles différents~:
      sphères  pour {\tt wn} et losanges pour {\tt cobra}.
\item \textbf{3d option -density}~:
      representer la densité d'échantillons dans un sous-volume avec
      une palette gris clair/obscur avec {\tt paraview}, comme cela on voit
      quels mélanges apparaissent plus souvent.
      Le nuage de point avec opacite=0.05 est similaire, mais si gros nuage (~4000x3000),
      c'est plus efficace de passer en "continu" sur un maillage regulier (~30x30 P1),
      et plus precis que d'echantillonner (~400x400).
\item \textbf{3d option -cut}~: coupe suivant un plan arbitraitre de la densite ou de l'échantillon
\item \textbf{visu 3d de gros .pal ou .pig avec .vtk (2e idee)}~: 
      on peut créer un pseudo-maillage de points avec juste les coords des pigments
      et utiliser une glyph de sphere.
      Le problème reste la couleur~: la fonction de transfert de paraview
      prend en argument un champs scalaire.
      Ici il faudrait un champs de vecteur~\mbox{$\boldsymbol{v}(a,b,j)=(a,b,j)$}
      comme donnée.
      La doc parle de {\tt color map data}, mais je n'ai pas compris.
      Il y a du 
      \href{https://discourse.paraview.org/t/volume-viz-with-multi-component-color-opacity-mapping-in-paraview-5-6-0-rc2/649}
           {multi-component mapping},
      mais ca semble etre pour l'opacite.
      Meme chose pour
      \href{https://discourse.paraview.org/t/shading-one-variable-with-the-values-of-another-volume-rendering/2838}
           {shading and volume rendering}.
      Donc pas clair pour l'instant.
\item \textbf{deduire} 
      automatiquement une palette possible pour une peinture ou un
      groupe de peintures donnees. 
      Le nuage de pixels d'une peinture est transforme en une enveloppe convexe~:
      pour chacun de ses sommets pointus, on cherche en son voisinage petit (eps)
      le pigment d'une collection qui minimise l'ecart type~: c'est le pigment
      qui représente la pointe. Ainsi, l'enveloppe convexe de tels pigments est
      proche de celle du nuage. Si on a pas de pigment pres de la pointe,
      c'et qu'il y a eu melange (ex: complementaire bleu n'apparait pas pure),
      on peut chercher un pig a l'ext, en augmentant le chroma ou la lumiere,
      mais sans retrecir l'enveloppe convexe.
\item ajouter les options UCS etc de conversion rgb2jab et inversement~:
      est-ce que ca change bcp le résultat~?
\item afficher l'effet d'un illuminant sur un cercle de teinte ou sur une palette~:
      notion de mélange, mais est-ce utile ici~?
\item logiciel de visu~3d plus rapide~: x3d~? view3dscene
      Depuis \href{http://people.csail.mit.edu/jaffer/Color/Dictionaries#winsor-newton}
                 {Jaffer}~: visu au format wrl
      \begin{verbatim}
      wget http://people.csail.mit.edu/jaffer/Color/winsor-newton.wrl
      view3dscene winsor-newton.wrl
      \end{verbatim}
      On pourrait générer wrl automatiquement en batch avec paraview puis
      utiliser view3dscene pour le regarder~?
      Les figs latex pouraient avoir un hyperlien sur un .wrl et qui lance view3dscene.
\end{itemize}
% *************************************************
% biblio
% *************************************************
%\vfill\clearpage %flushfigures
\bibliographystyle{plainnat}
\bibliography{biblio,Sar-2023-palette-software-autobib}
\end{document}
