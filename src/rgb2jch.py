# input:
#   rgb in hexa, as three [0:255] components
# output:
#   jch : j, c in [0:100], h in degrees [0:360]
# usage
#   python thisfile.py <rgb>
# example:
#   python thisfile.py  ffa134
#   python thisfile.py #ffa134
# note:
#   the '#' at the begining of rgb is optional
#
import sys
from math import *
from colorspacious.conversion import cspace_convert

#print('argv=',sys.argv)
if len(sys.argv) == 1:
     sys.stderr.write ('usage: python thisfile.py <rgb>\n');
     exit(1)

rgb_string = sys.argv[1]
rgb_string = rgb_string.lstrip('#')
#print (f'arg: rgb_string={rgb_string}')
n = len (rgb_string)
if n == 6:
    rgb_string = '0x' + rgb_string
elif n == 8 and rgb_string[0] == '0' and rgb_string[1] == 'x':
    True
else:
    sys.stderr.write (f'ERROR: rgb255 argument "{rgb_string}" should have 6 hexa digits, e.g. "0xa2b5c0" or "a2b5c0"\n');
    exit(1);

#print (f'hexa: rgb_string={rgb_string}')
rgb_value = int(rgb_string, 16)
#print (f'value: rgb_value={rgb_value}')
b = rgb_value % 256
g = ((rgb_value - b)/256) % 256
r =  (rgb_value - b - 256*g)/(256*256)
rgb = [r, g, b]
#print (f'value: rgb={rgb}')
jch = cspace_convert (rgb, "sRGB255", "JCh")
print (f'{jch[0]:19.15f} {jch[1]:19.15f} {jch[2]:19.15f}')

