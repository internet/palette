# input:
#   lch : cielab in polar (degrees)
# output:
#   jch : ciecam in polar (degrees)
# usage
#   python thisfile.py l c h
# example:
#   python thisfile.py 20 19 180
#
import sys
from math import *
from colorspacious.conversion import cspace_convert
from libpalette import *

#print('argv=',sys.argv)
if len(sys.argv) <= 3:
   sys.stderr.write ('usage: python thisfile.py <j> <c> <h>\n');
   exit(1)

l = float(sys.argv[1])
c = float(sys.argv[2])
h = float(sys.argv[3])

lch = [l, c, h]
lab = xch2xab (lch)
jch = cspace_convert (lab, "CIELab", "JCh")

print(f'# system ciejch')
print(f'{jch[0]} {jch[1]} {jch[2]}')
