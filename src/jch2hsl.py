# input:
#   jch : ciecam in polar (degrees)
# output:
#   rgb
# usage
#   python thisfile.py l c h
# example:
#   python thisfile.py 20 19 180
#
import sys
from math import *
from colorspacious.conversion import cspace_convert
from colormath.color_objects import LabColor, XYZColor, HSLColor
from colormath.color_conversions import convert_color
from libpalette import *

#print('argv=',sys.argv)
if len(sys.argv) <= 3:
   sys.stderr.write ('usage: python thisfile.py <j> <c> <h>\n');
   exit(1)

j = float(sys.argv[1])
c = float(sys.argv[2])
h = float(sys.argv[3])

jch = [j, c, h]
lab = cspace_convert (jch, "JCh", "CIELab")
lab_obj = LabColor (lab[0], lab[1], lab[2])
xyz_obj = convert_color (lab_obj, XYZColor)
hsl_obj = convert_color(xyz_obj, HSLColor)
hsl     = hsl_obj.get_value_tuple()
print (f'{hsl[0]:19.15f} {100*hsl[1]:19.15f} {100*hsl[2]:19.15f}')

