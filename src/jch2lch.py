# input:
#   jch : ciecam in polar (degrees)
# output:
#   lch : cielab in polar (degrees)
# usage
#   python thisfile.py l c h
# example:
#   python thisfile.py 20 19 180
#
import sys
from math import *
from colorspacious.conversion import cspace_convert

#print('argv=',sys.argv)
if len(sys.argv) <= 3:
   sys.stderr.write ('usage: python thisfile.py <j> <c> <h>\n');
   exit(1)

j = float(sys.argv[1])
c = float(sys.argv[2])
h = float(sys.argv[3])

jch = [j, c, h]
lab = cspace_convert(jch, "JCh", "CIELab")
l2 = lab[0]
a  = lab[1]
b  = lab[2]
c2 = sqrt(a**2 + b**2)
h2 = atan2 (b,a)*(360./(2*pi))
if (h2 < 0):
  h2 += 360

print(f'# system cielch')
print(f'{l2} {c2} {h2}')
