# TODO:
# * charger le fichier des pigments .gdat avec rgb
# * arcs de cercles verticaux pour
#   - les couleurs extra spectrales 
#   - les couleurs uniques
#
from math import *
import re;
import pathlib
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

from paraview.simple import *
#paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# utilities
# ----------------------------------------------------------------
def rgb2color (rgb_arg):
    if isinstance(rgb_arg, int):
        rgb = rgb_arg          # arg is already an int
    else:
        rgb = int(rgb_arg, 16) # arg is hexa string eg 0xaabbcc -> conversion to int
    b = rgb % 256
    g = ((rgb - b)/256) % 256
    r =  (rgb - b - 256*g)/(256*256)
    return [r/255., g/255., b/255.]

# similar, probably more general: strip '#' and supports 16 & 32 bits basis
def html_hexa_to_color(value):
    # example '#ff0000'       --> [1, 0, 0]
    #         '#ffff00000000' --> [1, 0, 0]
    value = value.lstrip('#') 
    n = len (value)
    if n == 6:
      vmax = 255.
    else:
      vmax = 65535.
    return tuple(int(value[i:i + n // 3], 16)/vmax for i in range(0, n, n // 3))

def palette_view (resolution, camera_position, camera_focal_point, camera_view_up, camera_parallel_scale):
    from paraview.simple import GetLayout, GetActiveViewOrCreate
    #print('TRACE palette_view...')
    layout = GetLayout()
    layout.SetSize (resolution[0], resolution[1])
    render  = GetActiveViewOrCreate ('RenderView')
    render.CameraParallelProjection  = 1
    render.OrientationAxesVisibility = 0
    render.CameraPosition      = camera_position
    render.CameraFocalPoint    = camera_focal_point
    render.CameraViewUp        = camera_view_up
    render.CameraParallelScale = camera_parallel_scale

def palette_save (resolution, image_file):
    from paraview.simple import GetLayout, GetActiveViewOrCreate, SaveScreenshot
    #print('TRACE palette_save...')
    layout = GetLayout()
    layout.SetSize (resolution[0], resolution[1])
    render  = GetActiveViewOrCreate ('RenderView')
    SaveScreenshot (image_file, render, ImageResolution=resolution)
    print(f'! file "{image_file}" created')
    #print('TRACE palette_save done')

def show_cousin_list (renderView1, position_list, name):
    if len(position_list) <= 1:
        return
    # paraview polyline requires a flat list of coords
    flat_coord_list = []
    for position in position_list:
        for coord in position:
            flat_coord_list.append (coord)
    polyLineSource1 = PolyLineSource (registrationName=name + '_cousins_polyline')
    polyLineSource1.Points = flat_coord_list

    polyLineSource1Display = Show (polyLineSource1, renderView1, 'GeometryRepresentation')
    polyLineSource1Display.AmbientColor = [0, 0, 0]
    polyLineSource1Display.DiffuseColor = [0, 0, 0]
    polyLineSource1Display.LineWidth = 6


# ----------------------------------------------------------------
# main visualization from a .gdat file
# ----------------------------------------------------------------
def paraview_palette (process_pair_list, option):
    # ----------------------------------------------------------------
    # create new layout object
    # ----------------------------------------------------------------
    layout = CreateLayout(name='palette')

    # ----------------------------------------------------------------
    # setup views used in the visualization
    # ----------------------------------------------------------------
    renderView1 = CreateView('RenderView')
    renderView1.CameraParallelProjection = 1
    if option.get('stereo') != None and option['stereo']:
        renderView1.StereoType = 'Anaglyph' # 'Anaglyph' or 'Red-Blue' for stereo ; default is 'Crystal Eyes'
    else:
        renderView1.StereoType = 'None'
    renderView1.ServerStereoType = 'Same As Client'
    #renderView1.ViewSize = window_size # aspect ratio bug with this setting
    renderView1.AxesGrid = 'GridAxes3DActor'
    renderView1.CenterOfRotation = [-0.050273895263671875, -0.25, -0.19999998807907104]
    renderView1.CameraPosition = [4.109290619471196, -3.7369946150241438, 1.1982103519634373]
    renderView1.CameraFocalPoint = [-0.050273895263671285, -0.24999999999999925, -0.19999998807907096]
    renderView1.CameraViewUp = [-0.1367335742372694, 0.22383306465180425, 0.9649884397467581]
    renderView1.CameraFocalDisk = 1.0
    renderView1.CameraParallelScale = 1.4506824921708834
    renderView1.AxesGrid.Visibility = 0
    renderView1.AxesGrid.XTitle = 'a'
    renderView1.AxesGrid.YTitle = 'b'
    renderView1.AxesGrid.ZTitle = 'J'
    layout.AssignView (0, renderView1)
    SetActiveView(None)
    
    # ----------------------------------------------------------------
    # create a unit box outline
    # ----------------------------------------------------------------
    show_box = False
    if show_box:
        box1 = Box(registrationName='Box1')
        box1.Center = [0.0, 0.0, 0.0]
        box1.XLength = 2.0
        box1.YLength = 2.0
        box1.ZLength = 2.0
        
        # show data from box1
        box1Display = Show(box1, renderView1, 'GeometryRepresentation')
        box1Display.Representation = 'Outline'
        box1Display.ColorArrayName = [None, '']
        box1Display.SelectTCoordArray = 'TCoords'
        box1Display.SelectNormalArray = 'Normals'
        box1Display.SelectTangentArray = 'None'
        box1Display.OSPRayScaleArray = 'Normals'
        box1Display.OSPRayScaleFunction = 'PiecewiseFunction'
        box1Display.SelectOrientationVectors = 'None'
        box1Display.ScaleFactor = 0.1
        box1Display.SelectScaleArray = 'None'
        box1Display.GlyphType = 'Arrow'
        box1Display.GlyphTableIndexArray = 'None'
        box1Display.GaussianRadius = 0.005
        box1Display.SetScaleArray = ['POINTS', 'Normals']
        box1Display.ScaleTransferFunction = 'PiecewiseFunction'
        box1Display.OpacityArray = ['POINTS', 'Normals']
        box1Display.OpacityTransferFunction = 'PiecewiseFunction'
        box1Display.DataAxesGrid = 'GridAxesRepresentation'
        box1Display.PolarAxes = 'PolarAxesRepresentation'
        box1Display.SelectInputVectors = ['POINTS', 'Normals']
        box1Display.WriteLog = ''
        # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
        box1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
        # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
        box1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
    
    # ----------------------------------------------------------------
    # create a unit transparent sphere
    # ----------------------------------------------------------------
    show_sphere = False
    if option.get('circle-resol') != None: 
      circle_resol = option['circle-resol']
    else:
      circle_resol = 16
    if show_sphere:
        sphere1 = Sphere(registrationName='Sphere1')
        sphere1.Center = [0, 0, 0]
        sphere1.Radius = 1
        sphere1.ThetaResolution = circle_resol
        sphere1.PhiResolution   = circle_resol
        
        # show data from sphere1
        sphere1Display = Show(sphere1, renderView1, 'GeometryRepresentation')
        
        # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
        sphere1Display.ScaleTransferFunction.Points = [-0.9749279022216797, 0.0, 0.5, 0.0, 0.9749279022216797, 1.0, 0.5, 0.0]
        
        # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
        sphere1Display.OpacityTransferFunction.Points = [-0.9749279022216797, 0.0, 0.5, 0.0, 0.9749279022216797, 1.0, 0.5, 0.0]
    
    # ----------------------------------------------------------------
    # create 3 horizontal ellises
    # ----------------------------------------------------------------
    z_list = [0,  0.5, -0.5]
    for z in z_list:
        ellipse1 = Ellipse(registrationName='ellipse_horizontal')
        ellipse1.Center = [0.0, 0.0, z]
        ellipse1.MajorRadiusVector = [sqrt(1-z**2), 0.0, 0.0]
    
        # show data in view
        ellipse1Display = Show(ellipse1, renderView1, 'GeometryRepresentation')
    
        # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
        ellipse1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.9900000095367432, 1.0, 0.5, 0.0]
        
        # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
        ellipse1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.9900000095367432, 1.0, 0.5, 0.0]
        
        # change solid color
        ellipse1Display.DiffuseColor = [0.0, 0.0, 0.0]
    
    # ----------------------------------------------------------------
    # create 2 vertical ellipses
    # ----------------------------------------------------------------
    # TODO: shared with gnuplot => put it in a data file
    # extra-spectral & uniques hues, in degres
    # rgb is obtained as:
    #   <rgb> <-- python hue2rgb.py <hue>
    # but chroma and luminosity are not well adjusted, so fix it by hand
    unique_color_line_width = 2.0
    hue_extra_spectral_violet = -66
    rgb_extra_spectral_violet = 0x815da7  # as PV15
    hue_extra_spectral_red    = 24
    rgb_extra_spectral_red    = 0xdc2833 # as PR108
    # unique red is very close to extra_spectral red: merged
    #hue_unique_red           = 20.14
    #rgb_unique_red           = 0xdc2833 # as PR108
    hue_unique_yellow         = 90
    rgb_unique_yellow         = 0xfee815 # as PY3
    hue_unique_green          = 164.25
    rgb_unique_green          = 0x9157   # as PG36
    hue_unique_blue           = 237.53
    rgb_unique_blue           = 0x61a0   # as PB15:3
    
    rotate_rgb_list = [
        ['red_extraspectral',    [90,  0, hue_extra_spectral_red],    rgb_extra_spectral_red],
        ['yellow_unique',        [90,  0, hue_unique_yellow],         rgb_unique_yellow],
        ['green_unique',         [90,  0, hue_unique_green],          rgb_unique_green],
        ['blue_unique',          [90,  0, hue_unique_blue],           rgb_unique_blue],
        ['violet_extraspectral', [90,  0, hue_extra_spectral_violet], rgb_extra_spectral_violet],
    ]
    
    for name, rotate, rgb in rotate_rgb_list:
        color = rgb2color (rgb)
        hue   = rotate[2]
        hue_rad_clip = (2*pi/360.)*(hue + 180)
    
        ellipse2 = Ellipse(registrationName=name+'_ellipse_vertical')

        # create a new 'Transform'
        transform2 = Transform(registrationName=name+'_ellipse_vertical_transform', Input=ellipse2)
        transform2.Transform = 'Transform'
        transform2.Transform.Rotate = rotate
        transform2.Transform.Scale = [1, 1, 0]
        
        # show data in view
        transform2Display = Show(transform2, renderView1, 'GeometryRepresentation')
        transform2Display.AmbientColor = color
        transform2Display.DiffuseColor = color
    
        # create a new 'Clip'
        clip1 = Clip(registrationName=name+'_ellipse_vertical_clip', Input=transform2)
        clip1.ClipType.Normal = [cos(hue_rad_clip), sin(hue_rad_clip), 0]
        clip1.Scalars = ['POINTS', '']
        clip1.ClipType = 'Plane'
        clip1.HyperTreeGridClipper = 'Plane'
        clip1.Scalars = [None, '']
        
        # show data in view
        clip1Display = Show(clip1, renderView1, 'UnstructuredGridRepresentation')
        clip1Display.LineWidth = unique_color_line_width
        clip1Display.AmbientColor = color
        clip1Display.DiffuseColor = color
        
        # hide intermediate data
        Hide(ellipse2,   renderView1)
        Hide(transform2, renderView1)
    
    # ----------------------------------------------------------------
    # create a unit gray cylinder
    # ----------------------------------------------------------------
    line1 = Line (registrationName='gray_axis')
    line1.Resolution = 20
    # Properties modified on line1
    line1.Point1 = [0, 0, -1]
    line1.Point2 = [0, 0,  1]
    
    # show data in view
    line1Display = Show(line1, renderView1, 'GeometryRepresentation')
    line1Display.AmbientColor = [0,0,0]
    line1Display.DiffuseColor = [0,0,0]
    
    # create a new 'Tube'
    tube1 = Tube(registrationName='gray_axis_cylinder', Input=line1)
    tube1.NumberofSides = 16
    tube1.Radius = 0.09
    tube1.Capping = 0
    tube1.Scalars = [None, '']
    tube1.Vectors = [None, '1']
  
    # show data in view
    tube1Display = Show (tube1, renderView1, 'GeometryRepresentation')
    tube1Display.Opacity = 0.5

    # set scalar coloring
    ColorBy (tube1Display, ('POINTS', 'Texture Coordinates', 'Magnitude'))
    
    textureCoordinatesLUT = GetColorTransferFunction ('TextureCoordinates')
    UpdateScalarBarsComponentTitle (textureCoordinatesLUT, tube1Display)
    HideScalarBarIfNotNeeded (textureCoordinatesLUT, renderView1)
    textureCoordinatesLUT.ApplyPreset ('Grayscale', True)

    if option.get('stereo') != None and option['stereo']:
        Hide (tube1, renderView1)
    else:
        # Hide (line1, renderView1)
        True 
    # ----------------------------------------------------------------
    # render all pigments or samples from an image
    # ----------------------------------------------------------------
    if False: # TOCLEAN
        if option.get('glyph-pig') != None and option['glyph-pig'] == 'box':
            glyph_pig = 'box'
        else:
            glyph_pig = 'sphere'
    sphere_size = 0.1
    if option.get('scale-pig') != None:
        sphere_size *= option['scale-pig']
    box_size    = 0.08
    box_scale   = 1
    if option.get('scale-pix') != None:
        box_scale *= option['scale-pix']
    box_size *= box_scale
    box_opacity = 0.2
    if option.get('opacity') != None:
        box_opacity = option['opacity']

    xmax = 100.  # max value fr j a b
    for in_name, category, hexa_color_pix, glyph_pig in process_pair_list:
        suffix = pathlib.Path(in_name).suffix
        color_pix = html_hexa_to_color (hexa_color_pix)
        print(f'TRACE in_name = {in_name} glyph_pig = \"{glyph_pig}\"')
        #print(f'TRACE category = {category} suffix = \"{suffix}\"')
        if suffix == '.vtk':
            if category == 'data':
                # vtk: sample from an image with glyph
                #print(f'TRACE vtk = \"{suffix}\" started')
                # create a new 'Legacy VTK Reader'
                in_vtk = LegacyVTKReader(registrationName=in_name, FileNames=[in_name])
                
                # get color transfer function/color map for 'tensor_norm'
                tensor_normLUT = GetColorTransferFunction('tensor_norm')
                # get opacity transfer function/opacity map for 'tensor_norm'
                tensor_normPWF = GetOpacityTransferFunction('tensor_norm')
                
                # show data in view
                in_vtk_display = Show(in_vtk, renderView1, 'UnstructuredGridRepresentation')
    
                # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
                in_vtk_display.ScaleTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]
                # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
                in_vtk_display.OpacityTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]
                # show color bar/color legend
                in_vtk_display.SetScalarBarVisibility(renderView1, True)
                
                # get 2D transfer function for 'tensor_norm'
                tensor_normTF2D = GetTransferFunction2D('tensor_norm')
                
                # create a new 'Tensor Glyph'
                tensorGlyph1 = TensorGlyph(registrationName=in_name+'_glyph', Input=in_vtk, GlyphType='Box')
                tensorGlyph1.ScaleFactor = 0.08*box_scale
                tensorGlyph1.Tensors = ['POINTS', 'tensor']
                tensorGlyph1.Scalars = ['POINTS', 'tensor_norm']
     
                # show data in view
                tensorGlyph1Display = Show(tensorGlyph1, renderView1, 'GeometryRepresentation')
                tensorGlyph1Display.Opacity = box_opacity
                tensorGlyph1Display.AmbientColor = color_pix
                tensorGlyph1Display.DiffuseColor = color_pix
    
                # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
                tensorGlyph1Display.ScaleTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]
                # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
                tensorGlyph1Display.OpacityTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]
                
                # hide data in view
                Hide(in_vtk, renderView1)
                # show color bar/color legend
                tensorGlyph1Display.SetScalarBarVisibility(renderView1, True)
                # turn off scalar coloring
                ColorBy(tensorGlyph1Display, None)
                # Hide the scalar bar for this color map if no visible data is colored by it.
                HideScalarBarIfNotNeeded(tensor_normLUT, renderView1)

            elif category == 'hull':
                # vtk: mesh of the hull
                in_vtk_face = LegacyVTKReader (registrationName=f'{in_name} faces', FileNames=[in_name])
                in_vtk_face_display = Show (in_vtk_face, renderView1, 'UnstructuredGridRepresentation')
                ColorBy(in_vtk_face_display, None)
                in_vtk_face_display.Representation = 'Surface'
                in_vtk_face_display.AmbientColor = color_pix
                in_vtk_face_display.DiffuseColor = color_pix
                in_vtk_face_display.Opacity      = 0.05

                in_vtk_edge = LegacyVTKReader (registrationName=f'{in_name} edges', FileNames=[in_name])
                in_vtk_edge_display = Show (in_vtk_edge, renderView1, 'UnstructuredGridRepresentation')
                ColorBy (in_vtk_edge_display, None)
                in_vtk_edge_display.Representation = 'Wireframe'
                in_vtk_face_display.AmbientColor = color_pix
                in_vtk_edge_display.DiffuseColor = color_pix
                in_vtk_edge_display.LineWidth = 2.0
                in_vtk_edge_display.Opacity   = 1.0

        else: # gdat file
            f = open (in_name, 'r')
            print(f'! load \"{in_name}\"')
            i_sample = 0
            cousin_list = []
            cousin_list_name = 'noname'
            while True:
                line = f.readline()
                if len(line) == 0:
                    # end of file
                    break
                line = re.sub(r'#.*?\n', '', line)
                line = re.sub(r'\n', '', line)
                if len(line) == 0:
                    # empty line: separates cousin lists, so reset it 
                    show_cousin_list (renderView1, cousin_list, cousin_list_name)
                    cousin_list = []
                    continue
                arg = line.split()
                pigment_name = arg[0]
                jab = [float(arg[1]), float(arg[2]), float(arg[3])]
                hue = (360./(2*pi))*atan2 (jab[2], jab[1])
                rgb = arg[4]
            
                # paraview data:
                pigment_position = [jab[1]/xmax, jab[2]/xmax, 2*jab[0]/xmax-1]
                pigment_color    = rgb2color (rgb)
                cousin_list.append (pigment_position)
                if len(cousin_list) == 1:
                    cousin_list_name = pigment_name
         
                if category == 'pigment':
                    if glyph_pig == 'box':
                        # ----------------------------------------------------------------------
                        # for each pigment create a small rotated box at (a,b,j)
                        # ----------------------------------------------------------------------
                        box2 = Box(registrationName=pigment_name+'_box')
                        glyph_size = 1.6*sphere_size # 1.6 ~= (4*pi/3)^(1./3) : box has same volume as a sphere
                        box2.XLength = glyph_size
                        box2.YLength = glyph_size
                        box2.ZLength = glyph_size
                        box2Display = Show(box2, renderView1, 'GeometryRepresentation')

                        # create a new 'Transform'
                        transform2 = Transform(registrationName=pigment_name+'_box_transform', Input=box2)
                        transform2.Transform.Translate = pigment_position
                        transform2.Transform.Rotate    = [0, 0, hue]
                        transform2.Transform = 'Transform'
                        HideInteractiveWidgets (proxy=transform2.Transform)
                        
                        # show data in view
                        transform2Display = Show(transform2, renderView1, 'GeometryRepresentation')
                        transform2Display.AmbientColor = pigment_color
                        transform2Display.DiffuseColor = pigment_color
 
                        # hide data in view
                        Hide(box2, renderView1)

                    else: # glyph_pig == 'sphere':
                        # ----------------------------------------------------------------
                        # for each pigment create a small sphere at (a,b,j
                        # ----------------------------------------------------------------
             
                        #print(f'pigment {pigment_name}');
                        sphere2 = Sphere(registrationName=pigment_name+'_sphere')
                        glyph_size = sphere_size 
                        sphere2.Center          = pigment_position
                        sphere2.Radius          = glyph_size
                        sphere2.ThetaResolution = circle_resol
                        sphere2.PhiResolution   = circle_resol
                        
                        # --------------------------------------
                        # create a small sphere
                        # --------------------------------------
                        sphere2Display = Show(sphere2, renderView1, 'GeometryRepresentation')
                        sphere2Display.AmbientColor = pigment_color
                        sphere2Display.DiffuseColor = pigment_color
                    
                    # --------------------------------------
                    # create a '3D Text' for the pigent name
                    # --------------------------------------
                    a3DText2 = a3DText(registrationName=pigment_name+'_label')
                    a3DText2.Text = pigment_name
                    
                    # show data in view
                    a3DText2Display = Show(a3DText2, renderView1, 'GeometryRepresentation')

                    # create a new 'Transform'
                    transform4 = Transform(registrationName=pigment_name+'_label_transform', Input=a3DText2)
                    transform4.Transform = 'Transform'
                    # Properties modified on transform4.Transform
                    text_size   = sphere_size
                    text_position = [0,0,0]
                    if glyph_pig == 'box':
                        shift_xy = 0.5*glyph_size
                    else:
                        shift_xy = 1.0*glyph_size
                    text_position[0] = (1 + shift_xy)*pigment_position[0]
                    text_position[1] = (1 + shift_xy)*pigment_position[1]
                    text_position[2] = pigment_position[2] - 0.5*glyph_size
                    transform4.Transform.Translate = text_position
                    transform4.Transform.Rotate = [90, 0, hue]
                    transform4.Transform.Scale = [text_size, text_size, text_size]
                    # show data in view
                    transform4Display = Show(transform4, renderView1, 'GeometryRepresentation')
                    transform4Display.DiffuseColor = [0, 0, 0]
                    Hide(a3DText2, renderView1)
        
                elif category == 'data': # and not .vtk: sample from an image with individual boxes
                    # ----------------------------------------------------------------
                    # for each sample create a small box (TODO: remove this old code)
                    # ----------------------------------------------------------------
                    # show data in view
                    box1 = Box(registrationName='sample_'+str(i_sample+1)+'_box')
                    i_sample += 1
        
                    # Properties modified on box1
                    box1.Center  = pigment_position
                    box1.XLength = box_size
                    box1.YLength = box_size
                    box1.ZLength = box_size
                    
                    box1Display = Show(box1, renderView1, 'GeometryRepresentation')
                    box1Display.AmbientColor = pigment_color
                    box1Display.DiffuseColor = pigment_color

    # ----------------------------------------------------------------
    # update the view
    # ----------------------------------------------------------------
    # restore active view
    SetActiveView (renderView1)
    SetActiveSource (None)
    renderView1.Update()
    if __name__ == '__main__':
        # generate extracts
        SaveExtracts(ExtractsOutputDirectory='extracts')

