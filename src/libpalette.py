#!/usr/bin/env python3
#
# palette visualisation
#
# author: Pierre.Saramito@imag.fr
#
# creation date: 10 july 2023
#

#BEGIN_RST2MAN palette
#palette - a 3D painting color visualisation and image analysis
#
#Synopsis
#--------
#
#palette [*options*] [file[.pal|.pig|.jpg]]"
#
#Examples
#--------
#The following command shows two 2D views of a palette::
#
#        palette oil-evoy-chagall.pal
#
#and also a 3D view::
#
#        palette oil-evoy-chagall.pal -3d
#
#For analyzing photo of a painting::
#
#        palette 1974_marc-chagall_palette.jpg
#        palette 1974_marc-chagall_palette.jpg -3d
#        palette 1974_marc-chagall_palette.jpg -3d -hull
#
#The first and second commands visualize a sample of all the image pixels in the
#color space.
#The last command computes and represents also
#the approximate convex hull of the cloud of points:
#the node of this polyhedron is used to automatically furnish 
#an hint about the palette
#used by the painter and, eventually the color harmony considered.
#The result is stored in the file **1974_marc-chagall_palette.pal**.
#
#Description
#-----------
#This command is useful when studying a palette or a painting.
#
#File arguments
#--------------
#Several files of different types could be combined.
#At least one should be provided.
#
#file[.pal]
#       A palette file as a list of names of pigments
#
#file[.pig]
#       A full collection, i.e. data base of pigment
#
#file[.jpg]
#       An image file.
#
#file[.gdat]
#       A raw data file, as computed from a previous run.
#
#Language options
#----------------
#
#-fr | -en
#      Language used for color labels
#
#Image option
#------------
#-sample *int*
#       Image sample in horizontal and vertical direction.
#       When the **0** value is provided, all the pixels are used.
#
#Render options
#--------------
#-2d | -3d
#       2d plots with **gnuplot** or 3d plot with **paraview**.
#       When 2d, produces tree cut planes *ab*, *aj* and *bj*.
#
#-[no]hull
#       represents the approximate convex hull of the pixels of images
#
#-cut-hue *float*
#       For 2d only, produces two orthogonal rotated vertical cut
#       planes instead of *aj* and *bj*.
#
#-[no]stereo
#      Toggle to stereo when 3d
#
#-scale-pig *float*
#      Change the size of glyphs, eg circles/squares or spheres/boxes for
#      for the pigment representation.
#      Also, change the font size for pigment labels accordingly.
#
#-scale-pix *float*
#      Change the size of glyphs, eg circles/squares or spheres/boxes for image pixel samples.
#      Useful when large cloud of pixels are displayed.
#
#-scale *float*
#      Set both pigment and pixel scales.
#
#-glyph-pig *string*
#      Change the glyph: either "sphere" or "box" 
#      for the pigment representation.
#      and "sphere" is the default.
#      Box is useful when cousins are searched, otherwise sphere are more classic.
#      For **paraview** render only.
#
#-color-pix *string*
#      Set the pixel color: should be in html format, e.g. **#4af10b**.
#      It will apply starting to the next *.jpg* images.
#      Useful when several interlaced clouds of pixels are displayed:
#      they could be distinguished.
#      Default is black. For **paraview** render only.
#
#-opacity *float*
#      Set the pixel opacity.
#      Useful when large cloud of pixels are displayed.
#      For **paraview** render only.
#
#-circle-resol *int*
#      Change the radial resolution of radius of circles or sphere.
#
#-[no]vtk
#      Use a vtk data file for rendering image sample (this is the default). 
#      Each sample is represented by a small monochrome box.
#      Alternative **-novtk** is to render each box with the corresponding individual
#      color: it it limited to small values of the *sample* resolution parameter.
#
#-view *view-file[.py]*
#      Load a python view file for **paraview**.
#      The *view-file.py* file name should not contain underscore, but could contains minus signs.
#      The *view-file.py* file format is as::
#
#             resolution            = [835, 763]
#             camera_position       = [ 1.033, -5.73,  0.462]
#             camera_focal_point    = [ 0.671, -0.10, -0.161]
#             camera_view_up        = [-0.016,  0.11,  0.993]
#             camera_parallel_scale = 0.819
#
#      This feature is especially is useful in conjunction with the **-batch** option,
#      as allows to generate automatically in a *.jpg* file an exact **paraview** scene.
#      The camera position can be obtained in **paraview** from the *tools/start trace* menu,
#      and activating the *full trace camera adjustments* option.
#      Then, the *tools/stop trace* menu will display a listing of camera positions
#      that should be saved in the *view-file.py* file format.
#
#**-average**
#      Print on standard error the average values of the ciecam (j,a,b) components of the pixel
#      foor each image and then exit.
#
#**-average-dilute**
#      Assume a dilute colorchart image suite: for each image,
#      in a first pass, classify pixels by slices of lightness (20 slices),
#      then, in a second pass, choose the slice reaching the highest chroma,
#      and performs aveaging on this slice only.
#      Print on standard error the average values of the ciecam (j,a,b) components, as in the
#      previous option.
#
#-hull-prec *float*
#      control the precision of the convex hull
#
#-hull-nmax *int*
#      control the size of the convex hull
#
#-hull-fit *file[.pig]*
#      the vertices of the convex hull are adjusted to the closest pigment in the catalog
#      provided in argument
#
#-[no-]hull-vertex
#      the vertices of the convex hull are shown as pigments, with labels as "p*n*"
#
#Others options
#--------------
#-name *string*
#       Specifies the basename used for all output files
#
#-[no]clean
#       Clear temporary graphic files (this is the default).
#
#-[no]verbose
#       Turn on verbosity (this is the default).
#
#-[no]batch
#       Execute graphic command (nobatch is the default).
#       The **-batch** variant is useful
#       in conjunction with the **-noclean** option
#       in order to modify some render options by hand.
#       When using **paraview**, it runs **pvbatch** instead
#       and performs a screen capture, see also the **-view** option.
#
#-libdir *string*
#       Specifies the path for the lipalette.py and others auxilliary files.
#       This option is used internally for using the palette command prior
#       to its installation, eg for building its documentation.
#
#Pigment catalog file format
#---------------------------
#The *.pig* pigment catalog file provides a data base for a family of pigments.
#The file starts by the abbrev and the coordinate system specification, e.g.::
#
#    abbrev w
#    system ciejch
#
#The abbrev is used as an optional prefix for pigment names when combining
#several catalogs of pigments in a palette.
#The system could either be **ciejab** or **ciejch**,
#the CIE-2002 standard in cartesian or polar coordinates systems,
#or **cielab** or **cielch**,
#the CIE-1976 standard in cartesian or polar coordinates systems,
#or **rgb255**, the hexa 6 digit RGB coordinates system.
#Next, each line starts by the pigment identifier, followed by
#the coordinates, e.g. *j*, *a* and *b*.
#The remaining of the line is an optional comment, e.g. the full name 
#and some attributes such as index in the catalog,
#full name and transparency::
#
#    PR122      38      71       1      magenta quina           semitransparent
#    PR108      41      78      36      cadium red              opaque
#
#Note that for the **rgb255** coordinate system, the color values takes one column
#instead of three::
#
#    PR122      0xc52d6f                magenta quina           semitransparent
#    PR108      0xdc2833                cadium red              opaque
#
#Also, the **0x** prefix for the rgb hexa value is optional. 
#Do not use a **#**, as for html notations, since it is interpreted as a comment in the file.
#
#Palette file format
#-------------------
#The *.pal* palette file lists the pigments that constitutes a given palette.
#It starts by indicating one or several pigment files, followed by a list of
#pigments::
#
#    pigment watercolor-evoy.pig
#    PR122
#    PR108
#
#Single pigments are indicated one per line. 
#Some cousins pigments are listed on the same line: they will be linked
#by a black line on the graphic representations::
#
#    PO20 PY117 PY129 PG50
#    PR:NA PB72 PG26 PG7
#
#When combining several pigment catalogs, the file could be::
#
#    pigment acrylic-winsor-newton.pig
#    pigment acrylic-golden.pig
#    w/PR122
#    g/PR108
#
#END_RST2MAN palette
import sys, os, pathlib, subprocess, re
from typing import Any, Dict, List
from math import *

# =============================================================================
# 0. help for color conversion
# =============================================================================

def xch2xab (xch):
    x = xch[0]
    c = xch[1]
    h = xch[2] # degree
    a = c*cos((2*pi)/360.*h)
    b = c*sin((2*pi)/360.*h)
    return [x, a, b]

def xab2xch (xab):
    x = xab[0]
    a = xab[1]
    b = xab[2]
    c = sqrt(a**2. + b**2.)
    h = (360./(2*pi))*atan2(b, a) # degree
    if (h <    0.): h += 360.
    if (h >= 360.): h -= 360.
    return [x, c, h]

def rgb_hexa2rgb3 (rgb_string):
    rgb_string = rgb_string.lstrip('#')
    #print (f'arg: rgb_string={rgb_string}')
    n = len (rgb_string)
    if n == 6:
        rgb_string = '0x' + rgb_string
    elif n == 8 and rgb_string[0] == '0' and rgb_string[1] == 'x':
        True
    else:
        sys.stderr.write (f'ERROR: rgb255 argument "{rgb_string}" should have 6 hexa digits, e.g. "0xa2b5c0" or "a2b5c0"\n');
        exit(1);
    #print (f'hexa: rgb_string={rgb_string}')
    rgb_value = int(rgb_string, 16)
    #print (f'value: rgb_value={rgb_value}')
    b = rgb_value % 256
    g = ((rgb_value - b)/256) % 256
    r =  (rgb_value - b - 256*g)/(256*256)
    return [r, g, b]

# =============================================================================
# 1. pigment catalog handler
# =============================================================================
# globals:
pig_catalog_set: Dict[str, Any] = {}

def search_pig (full_name):
    splited_name = full_name.split ('/')
    pig = None
    if len(splited_name) == 1:
        name = full_name
        n_founded = 0
        for abbrev in pig_catalog_set:
            pig_catalog = pig_catalog_set [abbrev]
            pig_list = pig_catalog ['list']
            pig = pig_list.get (name)
            if pig != None:
                n_founded += 1
        if n_founded == 0:
            sys.stderr.write (f'ERROR: pigment "{name}" not founded in catalogs\n');
            exit(1)
        elif n_founded > 1:
            sys.stderr.write (f'ERROR: pigment "{name}" appears in several catalogs\n');
            exit(1)
    elif len(splited_name) == 2:
        abbrev = splited_name [0]
        name   = splited_name [1]
        pig_catalog = pig_catalog_set.get (abbrev)
        if pig_catalog == None:
            sys.stderr.write (f'ERROR: pigment "{full_name}" but no catalog abbrev \"{abbrev}\"\n');
            exit(1)
        pig_list = pig_catalog ['list']
        pig = pig_list.get (name)
    else:
        sys.stderr.write (f'ERROR: invalid pigment name "{full_name}"\n');
        exit(1)
    if pig == None:
        sys.stderr.write (f'ERROR: pigment "{full_name} not found"\n');
        exit(1)
    return pig

def search_pig_catalog (in_pig_catalog):
    for abbrev in pig_catalog_set:
        pig_catalog = pig_catalog_set [abbrev]
        if pig_catalog_set [abbrev] ['filename'] == in_pig_catalog:
            return pig_catalog_set [abbrev]
    return None

def load_pig_catalog (in_pig_catalog):
    from colorspacious.conversion import cspace_convert
    #sys.stderr.write(f'TRACE load_pig "{pig}"\n')
    global pig_catalog_set
    prev_pig_catalog = search_pig_catalog (in_pig_catalog)
    if prev_pig_catalog != None: return prev_pig_catalog # already loaded
    fd_pig_catalog  = open (in_pig_catalog, 'r')
    sys.stderr.write (f'! load \"{in_pig_catalog}\"\n')
    sys_name = None # no default system
    abbrev = None # no default system
    pig_list = None
    while True:
        line = fd_pig_catalog.readline()
        #sys.stderr.write (f'TRACE line=\"{line}\"\n');
        if len(line) == 0:
            # end of file
            #sys.stderr.write (f'TRACE eof: break\n');
            break
        line = re.sub(r'#.*?\n', '', line)
        line = re.sub(r'\n', '', line)
        if len(line) == 0:  
            # empty line
            #sys.stderr.write (f'TRACE empty line\n');
            continue
        arg = line.split()
        if arg[0] == 'system':
            #sys.stderr.write (f'TRACE system: len(pig_catalog_set) \"{len(pig_catalog_set)}\"\n');
            if len(arg) <= 1:
                sys.stderr.write (f'ERROR: "system" line statement with missing argument\n');
                exit(1)
            sys_name = arg[1]
            continue
        elif arg[0] == 'abbrev':
            if len(arg) <= 1:
                sys.stderr.write (f'ERROR: "abbrev" line statement with missing argument\n');
                exit(1)
            abbrev = arg[1]
            #sys.stderr.write (f'TRACE abbrev=\"{abbrev}\"\n');
            continue
        # else: a line containing pigment and coords
        #sys.stderr.write (f'TRACE get a pigment...\"\n');
        if sys_name == None:
            sys.stderr.write (f'ERROR: "system" line statement should precede pigment definitions\n');
            exit(1)
        if abbrev == None:
            sys.stderr.write (f'ERROR: "abbrev" line statement should precede pigment definitions\n');
            exit(1)
        if pig_list == None:
            # not yet initialized
            pig_list: Dict[str, Any] = {}
        #sys.stderr.write (f'TRACE suite: len(pig_catalog_set) \"{len(pig_catalog_set)}\"\n');
        name = arg[0]
        if sys_name != 'rgb255': 
            coor = [float(arg[1]), float(arg[2]), float(arg[3])]
            ilast = 4
        else:
            coor = arg[1]
            ilast = 2
        comment = ''
        for i in range(ilast,len(arg)):
            comment = f'{comment} {arg[i]}'
        #sys.stderr.write (f'TRACE {name} {comment}\n');
        if pig_list.get (name) != None:
            sys.stderr.write (f'ERROR: pigment \"{name}\" is defined twice\n');
            exit(1)
        match sys_name:
            case 'ciejch': # CIECAM02
                jch = coor
                jab = xch2xab (jch)
                rgb3 = cspace_convert (jch, 'JCh', 'sRGB255')
            case 'ciejab': # CIECAM02 also, in polar system
                jab = coor
                jch = xab2xch (jab)
                rgb3 = cspace_convert (jch, 'JCh', 'sRGB255')
            case 'cielab': # CIE-1976
                lab = coor
                # TODO do not work with data from artpigment.org
                jch = cspace_convert (lab, 'CIELab', 'JCh')
                jab = xch2xab (jch)
                #sys.stderr.write (f'TRACE lab={lab} -> jab={jab}\"\n');
                rgb3 = cspace_convert (lab, 'CIELab', 'sRGB255')
            case 'cielch': # CIE-1976, also in polar system
                lch = coor
                lab = xch2xab (lch)
                jch = cspace_convert (lab, 'CIELab', 'JCh')
                jab = xch2xab (jch)
                rgb3 = cspace_convert (lab, 'CIELab', 'sRGB255')
                #sys.stderr.write (f'TRACE lch={lch} -> lab={lab}\"\n');
                #sys.stderr.write (f'TRACE lch={lch} -> jch={jch}\"\n');
                #sys.stderr.write (f'TRACE lch={lch} -> jab={jab}\"\n');
            case 'rgb255': # expect hexa with 6 digits string, e.g. "a2b734"
                rgb_string = coor
                rgb3 = rgb_hexa2rgb3 (rgb_string)
                jch  = cspace_convert (rgb3, 'sRGB255', 'JCh')
                jab  = xch2xab (jch)
            case _: 
                sys.stderr.write (f'ERROR: unsupported pigment coordinate system \"{sys_name}\"\n');
                exit(1)
        for i in [0,1,2]:
            rgb3[i] = int(round(min(255, max(0, rgb3[i]))))
        rgb = int(rgb3[0]*65536 + rgb3[1]*256 + rgb3[2])
        pig: Dict[str, Any] = {}
        pig ['jab']     = jab
        pig ['rgb']     = rgb
        pig ['comment'] = comment
        pig_list [name] = pig

    fd_pig_catalog.close()
    pig_catalog: Dict[str, Any] = {}
    pig_catalog['list'] = pig_list
    pig_catalog['filename'] = in_pig_catalog
    pig_catalog['system'] = sys_name
    pig_catalog['abbrev']   = abbrev
    pig_catalog_set [abbrev] = pig_catalog
    return pig_catalog

# =============================================================================
# 2. file conversion
# =============================================================================
# image slice helper
def make_slice (im, n_sample):
    width  = im.size[0]
    height = im.size[1]
    if n_sample != 0:
        step_width    = max(1, int(1.*width/n_sample + 0.5));
        step_height   = max(1, int(1.*height/n_sample + 0.5));
        shift_width   = int(step_width/2.);
        shift_height  = int(step_height/2.);
    else:
        step_width    = 1
        step_height   = 1
        shift_width   = 0
        shift_height  = 0
    #sys.stderr.write(f'TRACE width        = {width}\n')
    #sys.stderr.write(f'TRACE height       = {height}\n')
    #sys.stderr.write(f'TRACE step_width   = {step_width}\n')
    #sys.stderr.write(f'TRACE step_height  = {step_height}\n')
    #sys.stderr.write(f'TRACE shift_width  = {shift_width}\n')
    #sys.stderr.write(f'TRACE shift_height = {shift_height}\n')
    return [width, height, step_width, step_height, shift_width, shift_height]

# image2numpy + numpy [slice] + numpy2jch : faster, without loop
#       [ok] https://www.geeksforgeeks.org/how-to-convert-images-to-numpy-array
#       [ok] https://www.w3schools.com/python/numpy/numpy_array_slicing.asp
#       [ok] https://colorspacious.readthedocs.io/en/latest/reference.html
# TODO rgb2rgb_value, jch2jab : applique une fct conversion au tableau numpy
#       [??] https://numpy.org/doc/stable/reference/generated/numpy.apply_along_axis.html
# TODO sortie dans un fichier:
#       [??] https://stackoverflow.com/questions/68639461/print-formatted-numpy-array
def image2rgb (prog, in_image, n_sample):
    from PIL import Image
    from numpy import asarray, transpose
    im = Image.open (in_image) # im is w*h
    px = im.load()
    rgb_full_data = asarray (im) # get a 3-index array [h,w,3] ie width & height are transposed
    rgb_full_data = transpose (rgb_full_data, [1,0,2])
    width, height, step_width, step_height, shift_width, shift_height = make_slice (im, n_sample)
    rgb_data = rgb_full_data [shift_width:width:step_width,shift_height:height:step_height,0:3]
    return rgb_data

def rgb2jch (prog, rgb_data):
    from colorspacious.conversion import cspace_convert
    jch_data = cspace_convert (rgb_data, 'sRGB255', 'JCh')
    return jch_data

def image2gdat (prog, in_image, n_sample, out_filename):
    #sys.stderr.write (f'TRACE image2gdat...\n')
    rgb_data = image2rgb (prog, in_image, n_sample)
    jch_data = rgb2jch   (prog, rgb_data)
    w, h, c = jch_data.shape
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    out.write (f'# file automatically created by {prog}\n')
    out.write (f'# image    = {in_image}\n')
    out.write (f'# n_sample = {n_sample}\n')
    out.write (f'# label  j a b  rgb\n')
    for iw in range (0, w):
        for ih in range (0, h):
            rgb = rgb_data [iw,ih]
            #sys.stderr.write (f'TRACE image2gdat rgb={rgb}\n')
            rgb_value = int(float(rgb[0])*65536 + float(rgb[1])*256 + rgb[2])
            jch = jch_data [iw,ih]
            jab = xch2xab (jch)
            out.write (f'-    {jab[0]} {jab[1]} {jab[2]}    0x{rgb_value:06x}\n\n')
    out.close()
    #sys.stderr.write (f'TRACE image2gdat done\n')

def image2vtk (prog, in_image, n_sample, out_filename):
    #sys.stderr.write (f'TRACE image2vtk...\n')
    rgb_data = image2rgb (prog, in_image, n_sample)
    jch_data = rgb2jch   (prog, rgb_data)
    w, h, c = jch_data.shape
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    out.write (f'# vtk DataFile Version 1.0\n')
    out.write (f'Unstructured Grid\n')
    out.write (f'ASCII\n')
    out.write (f'DATASET UNSTRUCTURED_GRID\n')
    out.write (f'POINTS {w*h} float\n')
    for iw in range(0,w):
        for ih in range(0,h):
            rgb = rgb_data [iw,ih]
            rgb_value = int(rgb[0]*65536 + rgb[1]*256 + rgb[2])
            jch = jch_data [iw,ih]
            jab = xch2xab (jch)
            out.write (f'{jab[1]/100.} {jab[2]/100.} {2.*jab[0]/100.-1.}\n')
    
    out.write (f'CELLS 0 0\n')
    out.write (f'CELL_TYPES 0\n')
    out.write (f'POINT_DATA {w*h}\n')
    out.write (f'SCALARS tensor_norm float\n')
    out.write (f'LOOKUP_TABLE default\n')
    for k in range(0,w*h,1):
        # or luminosity, or several indicators, nuance, intensity, ?
        out.write (f'0.5\n')
    out.write (f'TENSORS tensor float\n')
    for k in range(0,w*h,1):
        out.write (f'1 0 0\n')
        out.write (f'0 1 0\n')
        out.write (f'0 0 1\n')
    out.close()
    #sys.stderr.write (f'TRACE image2vtk done\n')


image2average_cood_shown = False

def image2average (prog, in_image, n_sample, dilute):
    import numpy as np
    #sys.stderr.write (f'TRACE image2average...\n')
    #   tint = obtained by a full average
    #   chroma and lightness: averaged by slices in "j",
    #       then select the "j" of max chroma and use irs chroma and lightness
    #
    rgb_data = image2rgb (prog, in_image, n_sample)
    jch_data = rgb2jch   (prog, rgb_data)
    w, h, c = jch_data.shape
    wh = w*h
    jab_final = [0, 0, 0]
    jch_final = [0, 0, 0]
    if not dilute:
         for iw in range(0,w):
             for ih in range(0,h):
                 rgb = rgb_data [iw,ih]
                 jch = jch_data [iw,ih]
                 jab = xch2xab (jch)
                 j,a,b = jab
                 jab_final[0] += j
                 jab_final[1] += a
                 jab_final[2] += b
         jab_final[0] /= wh
         jab_final[1] /= wh
         jab_final[2] /= wh
         jch_final[0] = jab_final[0]
         jch_final[1] = sqrt (jab_final[1]**2 + jab_final[2]**2)
         jch_final[2] = (360./(2*pi))*atan2 (jab_final[2], jab_final[1])
    else:
         #sys.stderr.write(f'TRACE dilute file = {in_image}\n')
         n_slice_j = 20
         idx_j_range = range(0,n_slice_j)
         j_average_tab = np.zeros(n_slice_j)
         c_maximum_tab = np.zeros(n_slice_j)
         h_average_tab = np.zeros(n_slice_j)
         count_tab     = np.zeros(n_slice_j)
         idx_j_cmax_glob = -1
         cmax_glob = -1
         for iw in range(0,w):
             for ih in range(0,h):
                 rgb = rgb_data [iw,ih]
                 jch = jch_data [iw,ih]
                 #sys.stderr.write(f'TRACE rgb = {rgb} -> jch = {jch}\n')
                 idx_j = int((jch[0]/100.)*n_slice_j);
                 if idx_j < 0 or idx_j > n_slice_j:
                     sys.stderr.write(f'FATAL: {idx_j} out of range 0:{n_slice_j}\n')
                     exit(1)
                 if (idx_j == n_slice_j):
                     idx_j = n_slice_j-1
                 j_average_tab [idx_j] += jch[0]
                 h_average_tab [idx_j] += jch[2]
                 #sys.stderr.write(f'TRACE h_average_tab[{idx_j}] += {h} = {h_average_tab [idx_j]}\n')
                 count_tab     [idx_j] += 1
                 if jch[1] > cmax_glob:
                     cmax_glob = jch[1]
                     c_maximum_tab [idx_j] = jch[1]
                     idx_j_cmax_glob = idx_j
         
         for idx_j in idx_j_range:
             if count_tab [idx_j] > 0:
                 j_average_tab [idx_j] /= count_tab [idx_j]
                 h_average_tab [idx_j] /= count_tab [idx_j]
             #sys.stderr.write(f'TRACE count_tab[{idx_j}] = {count_tab[idx_j]} : c_maximum_loc = {c_maximum_tab [idx_j]} h={h_average_tab [idx_j]}\n')
    
         count_min = int(0.40*wh/n_slice_j)
         #sys.stderr.write(f'TRACE count_min = {count_min}\n')
         if count_tab [idx_j_cmax_glob] > count_min:
             jmax = j_average_tab [idx_j_cmax_glob]
             cmax = c_maximum_tab [idx_j_cmax_glob]
             hmax = h_average_tab [idx_j_cmax_glob]
             #sys.stderr.write(f'TRACE strong global-max idx_j = {idx_j_cmax_glob} cmax = {cmax} hmax = {hmax}\n')
         else:
             # glob max fails for PO62 : use a yield
             jmax = -1
             cmax = -1
             hmax = -1
             idx_j_cmax = -1
             for idx_j in idx_j_range:
                 if count_tab [idx_j] > count_min:
                     if c_maximum_tab [idx_j] > cmax:
                         idx_j_cmax = idx_j
                         jmax = j_average_tab [idx_j]
                         cmax = c_maximum_tab [idx_j]
                         hmax = h_average_tab [idx_j]
             #sys.stderr.write(f'TRACE weak local-max : idx_j_cmax = {idx_j_cmax} cmax = {cmax} hmax = {hmax}\n')
    
         #sys.stderr.write(f'TRACE cmax = {cmax} hmax = {hmax}\n')
         jch_final[0] = jmax
         jch_final[1] = cmax
         jch_final[2] = hmax
         a = cmax*cos (((2*pi)/360.)*hmax)
         b = cmax*sin (((2*pi)/360.)*hmax)
         jab_final[0] = jmax
         jab_final[1] = a
         jab_final[2] = b
          
    basename = in_image.removesuffix ('.jpg')
    # jab:
    # print(f'{basename} {jab_final[0]} {jab_final[1]} {jab_final[2]}')
    # jch:
    global image2average_cood_shown
    if not image2average_cood_shown:
        image2average_cood_shown = True
        sys.stdout.write (f'abbrev a\n')
        sys.stdout.write (f'system ciejch\n')
        sys.stdout.write (f'# file j c h\n')
    #sys.stderr.write (f'TRACE image2average...\n')
    sys.stdout.write (f'{basename} {jch_final[0]} {jch_final[1]} {jch_final[2]}\n')
    #sys.stderr.write (f'TRACE image2average done\n')

def image2hull (prog, in_image, hull_opt, n_sample):
    # idee : entrer le nuage de pixels en coords 3D jab
    #        1. sortir son enveloppe convexe
    #        2. puis, pour chaque sommet, chercher le pigment le + proche
    #           variante : annee du tableau => pigment suffisamment ancien seulement
    #        3. en deduire la palette
    #
    # TODO:
    # simplification quand beaucoup de points :
    # reduction du nombre de faces/sommets du polygone convexe
    #         http://qhull.org/html/qh-faq.htm#simplex
    #      Wn : Minimum outside width of the hull.
    #           Points are added to the convex hull only if they are clearly outside of a facet.
    #           A point is outside of a facet if its distance to the facet is greater than 'Wn'.
    #           The normal value for 'Wn' is 'En'.
    #           If the user specifies pre‐merging and does not set 'Wn', than 'Wn' is set to the premerge 'Cn' and maxcoord*(1-An).
    #      Cn : Centrum  radius.
    #           If a centrum is less than n below a neighboring facet, Qhull merges one of the facets.
    #           If 'n' is negative or '-0', Qhull tests and merges facets after adding each point to the hull.
    #           This is called "pre‐merging". 
    #           If 'n' is positive, Qhull tests for convexity after constructing the hull ("post‐merging").
    #           Both pre‐ and post‐merging can be defined.
    #      PAn: Only the n largest facets are marked good for printing.
    #           Unless 'PG' is set, 'Pg' is automatically set.
    #      Fo : Print separating hyperplanes for unbounded, outer regions of the Voronoi diagram.
    #           The first line is the number of ridges.
    #           Then each hyperplane is printed, one per line.
    #           A line starts with the number of indices and floats.
    #           The first pair lists adjacent input sites, the next d floats are the normalized coefficients
    #           for the hyperplane, and the last float is the offset.
    #           The hyperplane is oriented toward 'QVn' (if defined), or the first input site of the pair.
    #           Use 'Tv' to verify that the hyperplanes are perpendicular bisectors.
    #           Use 'Fi' for bounded regions, and 'Fv' for the corresponding Voronoi vertices.
    #
    # doc interface python:
    #   https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.ConvexHull.html
    # demo enveloppe convexe 3d:
    #   https://stackoverflow.com/questions/63207496/how-to-visualize-polyhedrons-defined-by-their-vertices-in-3d-with-matplotlib-or
    #
    import numpy as np
    from scipy.spatial import ConvexHull, distance # interface to http://www.qhull.org
    
    #sys.stderr.write (f'TRACE hull {in_image}...\n');
    rgb_data = image2rgb (prog, in_image, n_sample)
    jch_data = rgb2jch   (prog, rgb_data)
    w, h, c = jch_data.shape
    ndx = w*h
    #sys.stderr.write (f'TRACE ndx={ndx}\n')
    idx = 0
    point = np.zeros((ndx, 3))
    for iw in range(0,w):
        for ih in range(0,h):
            rgb = rgb_data [iw,ih]
            rgb_value = int(float(rgb[0])*65536 + float(rgb[1])*256 + rgb[2])
            jch = jch_data [iw,ih] 
            j,a,b = xch2xab (jch)
            # point = [a b j] : all in [-1:1]
            point [idx] = [a/100., b/100., 2.*j/100.-1.]
            idx += 1

    #sys.stderr.write (f'point={point}\n')
    prec = hull_opt ['prec']     # distance : coord in [-1:1]
    nmax = hull_opt ['nmax']     # nb de facette max, si possible avec cette precision sur la distance
    opt = f'W{prec} C{prec} PA{nmax} Fo'
    #sys.stderr.write (f'opt="{opt}"\n')
    hull = ConvexHull (point, qhull_options=opt)
    sys.stderr.write (f'hull(nmax={nmax} and prec={prec}): {len(hull.vertices)} vertices from {len(hull.points)} points\n')
    #sys.stderr.write (f'hull len(point)={len(hull.points)}\n')
    #sys.stderr.write (f'hull area={hull.area}\n')
    #sys.stderr.write (f'hull volume={hull.volume}\n')
    #sys.stderr.write (f'hull len(simplicies)={len(hull.simplices)}\n')
    #sys.stderr.write (f'hull len(vertices)={len(hull.vertices)}\n')
    #sys.stderr.write (f'TRACE hull {in_image} done\n');
    return hull

def build_hull_ip2iv (hull):
    import numpy as np
    #sys.stderr.write (f'TRACE hull2gdat...\n');
    ndx = len(hull.points)
    nv  = len(hull.vertices)
    ip2iv = np.full (ndx, -1)
    for iv in range(0,nv):
        ip = hull.vertices [iv]
        ip2iv [ip] = iv
    return ip2iv

def hull2gdat (prog, hull, out_filename):
    #sys.stderr.write (f'TRACE hull2gdat...\n');
    ip2iv = build_hull_ip2iv (hull)
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    nc = len(hull.simplices)
    out.write (f'# edges of the {nc} sides of the convex hull\n')
    out.write (f'# pigment j a b\n')
    for ic in range(0,nc):
        cell = hull.simplices[ic]
        out.write (f'# {ic} cell\n')
        for loc_iv3 in range(0,4):
            loc_iv = loc_iv3 % 3
            ip = cell[loc_iv]
            p = hull.points[ip]
            iv = ip2iv [ip]
            pigname = f'p{iv}' 
            # point = [a b j] : all in [-1:1] => print jab as usual in .gdat
            out.write (f'{pigname} {100*(p[2]+1)/2.} {100*p[0]} {100*p[1]}\n')
        out.write ('\n')
    out.close()
    #sys.stderr.write (f'TRACE hull2gdat done\n');

def hull2vtk (prog, hull, out_filename):
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    nv = len(hull.vertices)
    out.write (f'# vtk DataFile Version 1.0\n')
    out.write (f'Unstructured Grid\n')
    out.write (f'ASCII\n')
    out.write (f'DATASET UNSTRUCTURED_GRID\n')
    out.write (f'POINTS {nv} float\n')
    for iv in range(0,nv):
        ip = hull.vertices [iv]
        p  = hull.points [ip]
        # point = [a b j] : all in [-1:1] for paraview
        out.write (f'{p[0]} {p[1]} {p[2]}\n')
    nc = len(hull.simplices)
    ip2iv = build_hull_ip2iv (hull)
    out.write (f'CELLS {nc} {4*nc}\n')
    for ic in range(0,nc):
        cell = hull.simplices[ic]
        iv0 = ip2iv [cell[0]]
        iv1 = ip2iv [cell[1]]
        iv2 = ip2iv [cell[2]]
        out.write (f'3 {iv0} {iv1} {iv2}\n')
    out.write (f'CELL_TYPES {nc}\n')
    for ic in range(0,nc):
        out.write ('5\n')
    out.write (f'POINT_DATA {nv}\n')
    out.write (f'SCALARS mesh float\n')
    out.write (f'LOOKUP_TABLE default\n')
    for iv in range(0,nv):
        out.write ('0\n')
    out.close()

def hull2pig (prog, hull, out_filename):
    #sys.stderr.write (f'TRACE hull2pig...\n')
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    nv  = len(hull.vertices)
    out.write (f'# the {nv} vertices of the convex hull\n')
    out.write (f'abbrev v\n')
    out.write (f'system ciejch\n')
    out.write (f'# pigment j c h comment\n')
    for iv in range(0,nv):
        ip = hull.vertices [iv]
        p  = hull.points [ip]
        # point = [a b j] : all in [-1:1] => re-convert as jab
        jab = [100*(p[2]+1)/2., 100*p[0], 100*p[1]]
        jch = xab2xch (jab)
        pigname = f'p{iv}' 
        out.write (f'{pigname} {jch[0]} {jch[1]} {jch[2]}\n')
    out.close()
    #sys.stderr.write (f'TRACE hull2pig done\n');

def distance3d (p,q):
    return sqrt ( (p[0]-q[0])**2 + (p[1]-q[1])**2 + (p[2]-q[2])**2 )
 
def hull2pal_fit (prog, hull, in_pig_catalog, out_filename):
    # ajust vertices of the hull to a pigment collection 
    #sys.stderr.write (f'TRACE hull2pal_fit...\n')
    pig_catalog = load_pig_catalog (in_pig_catalog)
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    rootname = out_filename.removesuffix ('.pal')
    out.write (f'pigment {in_pig_catalog}\n')
    out.write (f'#pigment {rootname}.pig\n')
    out.write (f'\n')
    nv  = len(hull.vertices)
    pal_list = []
    for iv in range(0,nv):
        out.write (f'#--------------\n')
        out.write (f'#p{iv}\n')
        out.write (f'#--------------\n')
        ip = hull.vertices [iv]
        p  = hull.points [ip]
        # point = [a b j] : all in [-1:1]
        pig_list = pig_catalog ['list']
        pig_dict: Dict[str, Any] = {}
        for name in pig_list:
            pig = pig_list [name]
            j,a,b   = pig['jab']
            # point = [a b j] : all in [-1:1]
            q = [a/100., b/100., 2.*j/100.-1.]
            pig_dict[name] = distance3d (p,q)
        sorted_pig_list = sorted (pig_dict, key=pig_dict.__getitem__)
        first = True
        for name in sorted_pig_list:
            d = pig_dict[name]
            comment = pig_list [name] ['comment']
            if first and not name in pal_list:
                pal_list.append (name)
                com = ''
            else:
                com = '#'
            out.write (f'{com}{name:<10}    # {d}  {comment}\n')
            first = False
        out.write (f'\n')

    out.close()
    sys.stderr.write (f'hull-fit: palette with {len(pal_list)} pigments\n')
    #sys.stderr.write (f'TRACE hull2pal_fit done\n')

# convert a .pig to .gdat for gnuplot
def pig2gdat (prog, in_pig_filename, out_filename):
    from colorspacious.conversion import cspace_convert
    pig_catalog = load_pig_catalog (in_pig_filename)
    #sys.stderr.write (f'TRACE pig_catalog = {pig_catalog}\n');
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    out.write (f'# file automatically created by {prog}\n')
    out.write('# name j a b  rgb l c h\n')
    pig_list = pig_catalog ['list']
    for name in pig_list:
        pig = pig_list [name]
        #sys.stderr.write (f'TRACE pig({name}) = {pig}\n');
        jab = pig['jab']
        rgb = pig['rgb']
        out.write (f'{name}    {jab[0]} {jab[1]} {jab[2]}    0x{rgb:06x}\n')
        # version using gnuplot hsv2rgv() is less accuate and slower:
        # jch = xab2xch (jab)
        # lab = cspace_convert (jch, 'JCh', 'CIELab')
        # lch = xab2xch (lab)
        # out.write (f'{name}    {jab[0]} {jab[1]} {jab[2]}    0x{rgb:06x}    {lch[0]} {lch[1]} {lch[2]}\n')
        out.write('\n\n')
    out.close()

def pal2gdat (prog, in_pal, out_filename):
    from colorspacious.conversion import cspace_convert
    pal = open (in_pal,   'r')
    sys.stderr.write (f'! load "{in_pal}"\n')
    out = open (out_filename, 'w')
    sys.stderr.write (f'! file "{out_filename}" created\n')
    have_material = False
    while True:
        line = pal.readline()
        if len(line) == 0:
            # end of file
            break
        line = re.sub(r'#.*?\n', '', line)
        line = re.sub(r'\n', '', line)
        if len(line) == 0:  
            # empty line
            continue
        arg = line.split()
        if len(arg) == 0:
            # empty line
            continue
        if arg[0] == 'pigment':
            in_pig_catalog = arg[1]
            in_pig_catalog = in_pig_catalog.removesuffix ('.pig')
            in_pig_catalog = in_pig_catalog + '.pig'
            load_pig_catalog (in_pig_catalog)
            out.write(f'# pigment {in_pig_catalog}\n')
            out.write(f'# name j a b rgb\n')
            have_material = True
            continue

        if not have_material:
            sys.stderr.write ('pal file: missing \"pigment\" statement\n');
            exit(1)

        for i in range(0,len(arg)):
            name = arg[i]
            pig = search_pig (name)
            if pig == None:
                sys.stderr.write (f'ERROR: undefined pigment \"{name}\"\n');
                exit(1)
            jab = pig['jab']
            rgb = pig['rgb']
            out.write (f'{name}    {jab[0]} {jab[1]} {jab[2]}    0x{rgb:06x}\n')   
            # version using gnuplot hsv2rgv() is less accuate and slower:
            # jch = xab2xch (jab)
            # lab = cspace_convert (jch, 'JCh', 'CIELab')
            # lch = xab2xch (lab)
            # out.write (f'{name}    {jab[0]} {jab[1]} {jab[2]}    0x{rgb:06x}    {lch[0]} {lch[1]} {lch[2]}\n')   

        out.write('\n\n')
    pal.close()
    out.close()

# =============================================================================
# 3. main program
# =============================================================================
def palette (argv, libdir):
    # configuration when install:
    # -------------------------------------------------------------------------
    # 1. Get options from command line
    # -------------------------------------------------------------------------
    #sys.stderr.write (f'TRACE argv={argv}\n')
    
    if len(argv) == 0:
        raise SystemExit (f'palette: unexpected empty command line')
    prog                = argv[0]
    image_suffix_list   = ['.jpg']
    suffix_list         = ['.pig', '.pal', '.gdat'] + image_suffix_list
    filename_list       = []
    process_list        = []
    toclean_list        = []
    filename_dict: Dict[str, Any] = {}
    color_pix           = 'default'
    main_rootname       = ''
    view_rootname       = ''
    tmpdir              = '/tmp'
    render_dim          = 2
    average             = False
    dilute              = False
    n_sample            = 400
    lang                = 'en'
    cut_hue             = 0
    scale_pig           = 1
    scale_pix           = 0.1
    glyph_pig           = 'sphere'
    opacity             = 0.1
    circle_resol        = 16
    in_hull_pig_catalog = ''
    stereo              = False
    vtk                 = True
    clean               = True
    verbose             = True
    batch               = False
    hull                = False
    hull_prec           = 0.1    # distance : coord in [-1:1]^3 for jab
    hull_nmax           = 12     # nb de facette max, si possible avec cette precision sur la distance
    hull_vertex_show    = False
    pdf_raster          = False
    pdf_raster_density  = 600
    USAGE               = f"""{prog}: usage: {prog}
[-fr|-en]
[-2d|-3d]
[-cut-hue float={cut_hue}]
[-average[-dilute]]
[-sample int={n_sample}]
[-scale-pig float={scale_pig}]
[-scale-pix float={scale_pix}]
[-glyph-pig string={glyph_pig}]
[-color-pix string={color_pix}]
[-opacity float={opacity}]
[-circle-resol int={circle_resol}]
[-hull-nmax int={hull_nmax}]
[-hull-prec float={hull_prec}]
[-hull-fit string]
[-pdf-raster-density int={pdf_raster_density}]
[-[no-]hull-vertex]
[-[no-]pdf-raster]
[-[no]batch]
[-[no]clean]
[-[no]verbose]
[-[no]stereo]
[-[no]vtk]
[-[no]hull]
[-name string]
[-libdir string]
[file[.pal|.jpg|.gdat]]*
"""
    USAGE = re.sub (r'\n', ' ', USAGE)
    if len(argv) <= 1:
        sys.stderr.write (f'{USAGE}\n')
        exit(0)
    i = 1
    while i < len(argv):
        arg = argv[i]
        i += 1
        if   arg == '-2d': render_dim = 2
        elif arg == '-3d': render_dim = 3
        elif arg == '-fr': lang = 'fr'
        elif arg == '-en': lang = 'en'
        elif arg ==   '-batch': batch = True
        elif arg == '-nobatch': batch = False
        elif arg ==   '-clean': clean = True
        elif arg == '-noclean': clean = False
        elif arg ==   '-verbose': verbose = True
        elif arg == '-noverbose': verbose = False
        elif arg ==   '-stereo': stereo = True
        elif arg == '-nostereo': stereo = False
        elif arg ==   '-vtk': vtk = True
        elif arg == '-novtk': vtk = False
        elif arg ==   '-hull': hull = True
        elif arg == '-nohull': hull = False
        elif arg ==   '-hull-vertex':    hull_vertex_show = True
        elif arg ==   '-no-hull-vertex': hull_vertex_show = False
        elif arg ==   '-average': average = True
        elif arg ==   '-pdf-raster': pdf_raster = True
        elif arg ==   '-no-pdf-raster': pdf_raster = False
        elif arg ==   '-average-dilute':
            average = True
            dilute  = True
        elif arg in ['-cut-hue', '-sample', '-scale', '-scale-pig', '-scale-pix', '-glyph-pig',
                     '-color-pix', '-opacity', '-circle-resol', '-name', '-view', 
                     '-hull-nmax', '-hull-prec', '-hull-fit', '-pdf-raster-density', '-libdir',]:
            if i >= len(argv):
                sys.stderr.write (f'{USAGE}\n')
                raise SystemExit (f'{prog}: missing value for option "{arg}"')
            value = argv[i]
            i += 1
            if   arg == '-cut-hue':      cut_hue = float(value)
            elif arg == '-sample':       n_sample = int(value)
            elif arg == '-scale-pig':    scale_pig = float(value)
            elif arg == '-scale-pix':    scale_pix = float(value)
            elif arg == '-scale':        scale_pig = scale_pig = float(value)
            elif arg == '-glyph-pig':    glyph_pig = value; sys.stderr.write (f'TRACE set glyph_pig={glyph_pig} global\n')
            elif arg == '-color-pix':    color_pix = value
            elif arg == '-opacity':      opacity = float(value)
            elif arg == '-circle-resol': circle_resol = int(value)
            elif arg == '-hull-nmax':    hull_nmax = int(value)
            elif arg == '-hull-prec':    hull_prec = float(value)
            elif arg == '-hull-fit':     in_hull_pig_catalog = value; hull = True
            elif arg == '-pdf-raster-density': pdf_raster_density = value; pdf_raster = True
            elif arg == '-libdir':       libdir = value
            elif arg == '-name':         main_rootname = value
            elif arg == '-view':
                orig_view_rootname = value
                #orig_view_rootname = os.path.basename (orig_view_rootname)
                orig_view_rootname = orig_view_rootname.removesuffix ('.py')
                if not os.path.exists (orig_view_rootname + '.py'):
                    raise SystemExit (f'{prog}: file "{orig_view_rootname}.py" not found')
                view_rootname = 'paletteview'
                view_filename = os.path.join (tmpdir, view_rootname + '.py')
                command = f'cp {orig_view_rootname}.py {view_filename}'
                if verbose:
                    sys.stderr.write (f'! {command}\n')
                result = subprocess.run (command, shell=True, check=True)
                if result.returncode != 0:
                    raise SystemExit (f'{prog}: cp command failed (status={result.returncode})')
                toclean_list.append (view_filename)
            else:
                sys.stderr.write (f'{USAGE}\n')
                raise SystemExit (f'{prog}: unexpected option "{arg}"')
        elif arg[0] == '-':
            sys.stderr.write (f'{USAGE}\n')
            raise SystemExit (f'{prog}: unexpected option "{arg}"')
        else:
            filename = arg
            if  filename_dict.get (filename) == None:
                filename_dict     [filename]: Dict[str, Any] = {}
            filename_dict [filename] ['color']     = color_pix
            filename_dict [filename] ['glyph-pig'] = glyph_pig
            sys.stderr.write (f'TRACE set glyph_pig={glyph_pig} for {filename}\n')
            filename_list.append (filename)
    
    #sys.stderr.write (f'TRACE filename_list={filename_list}\n')
    #sys.stderr.write (f'TRACE render_dim  = {render_dim}\n')
    if len(filename_list) == 0:
        raise SystemExit (f'{prog}: neither palette nor image file argument founded')
    
    # -------------------------------------------------------------------------
    # 2. Loop on filename_list and build .gdat or .vtk
    # -------------------------------------------------------------------------
    PLOT2PDF = f'plot2pdf'
    if pdf_raster:
        PLOT2PDF = f'plot2pdf -raster -raster-density {pdf_raster_density}'
    for filename in filename_list:
        color_pix = filename_dict [filename] ['color']
        glyph_pig = filename_dict [filename] ['glyph-pig']
        dirname  = os.path.dirname  (filename)
        basename = os.path.basename (filename)
        suffix   = pathlib.Path (basename).suffix
        rootname = basename.removesuffix (suffix)
        #sys.stderr.write (f'TRACE dirname ={dirname}\n')
        #sys.stderr.write (f'TRACE basename={basename}\n')
        #sys.stderr.write (f'TRACE rootname={rootname}\n')
        #sys.stderr.write (f'TRACE suffix  ={suffix}\n')
        if main_rootname == '':
            main_rootname = rootname
        if suffix != '':
            if not suffix in suffix_list:
                raise SystemExit (f'{prog}: unsupported file suffix "{suffix}"')
            if not os.path.exists (filename):
                raise SystemExit (f'{prog}: file "{filename}" not found')
        else:
            for s in suffix_list:
                path = filename + s
                if os.path.exists (path):
                    if suffix != '':
                        raise SystemExit (f'{prog}: ambiguous: both {path} and {filename}{suffix} founded')
                    suffix = s
        if suffix == '':
            raise SystemExit (f'{prog}: no file "{filename}" with suffix in {suffix_list}')
        # here, have the suffix
        filename = os.path.join (dirname, rootname + suffix)
        if not os.path.exists (filename):
            raise SystemExit (f'{prog}: file "{filename}" not found')
        if suffix == '.gdat':
            out_filename = filename
            process_list.append (out_filename)
            if  filename_dict.get (filename) == None:
                filename_dict     [filename]: Dict[str, Any] = {}
            filename_dict [filename] ['category']  = 'data'
            filename_dict [filename] ['color']     = color_pix
            filename_dict [filename] ['glyph-pig'] = glyph_pig
        elif suffix in image_suffix_list:
            if average:
                image2average (prog, filename, n_sample, dilute)
            else:
                if render_dim == 2:
                    out_filename = rootname + '.gdat'
                    image2gdat (prog, filename, n_sample, out_filename)
                else:
                    out_filename = rootname + '.vtk'
                    image2vtk  (prog, filename, n_sample, out_filename)
                if  filename_dict.get (out_filename) == None:
                    filename_dict     [out_filename]: Dict[str, Any] = {}
                filename_dict [out_filename] ['category']  = 'data'
                filename_dict [out_filename] ['color']     = color_pix
                filename_dict [out_filename] ['glyph-pig'] = glyph_pig
                toclean_list.append (out_filename)
                process_list.append (out_filename)
                if hull:
                    hull_opt = {'prec': hull_prec, 'nmax': hull_nmax}
                    hull = image2hull (prog, filename, hull_opt, n_sample)
                    # 1) output hull as a surface
                    if render_dim == 2:
                        hull_filename = rootname + '-hull-mesh.gdat'
                        hull2gdat (prog, hull, hull_filename)
                    else:
                        hull_filename = rootname + '-hull-mesh.vtk'
                        hull2vtk (prog, hull, hull_filename)
                    if  filename_dict.get (hull_filename) == None:
                        filename_dict     [hull_filename]: Dict[str, Any] = {}
                    filename_dict [hull_filename] ['category']  = 'hull'
                    filename_dict [hull_filename] ['color']     = color_pix
                    filename_dict [hull_filename] ['glyph-pig'] = glyph_pig
                    toclean_list.append (hull_filename)
                    process_list.append (hull_filename)
                    # 2) output also vertices of hull as .pig
                    pig_hull_filename = rootname + '-hull-vert.pig'
                    dat_hull_filename = rootname + '-hull-vert.gdat'
                    hull2pig (prog, hull, pig_hull_filename)
                    if hull_vertex_show:
                        pig2gdat (prog, pig_hull_filename, dat_hull_filename)
                        if  filename_dict.get (dat_hull_filename) == None:
                            filename_dict     [dat_hull_filename]: Dict[str, Any] = {}
                        filename_dict [dat_hull_filename] ['category']  = 'pigment'
                        filename_dict [dat_hull_filename] ['color']     = color_pix
                        filename_dict [dat_hull_filename] ['glyph-pig'] = glyph_pig
                        process_list.append (dat_hull_filename)
                        toclean_list.append (dat_hull_filename)
                        #toclean_list.append (pig_hull_filename)
                    # 3) fit vertices to a pigment catalog
                    if in_hull_pig_catalog != '':
                        pal_fit_hull_filename = rootname + '-hull-fit.pal'
                        dat_fit_hull_filename = rootname + '-hull-fit.gdat'
                        hull2pal_fit (prog, hull, in_hull_pig_catalog, pal_fit_hull_filename)
                        pal2gdat (prog, pal_fit_hull_filename, dat_fit_hull_filename)
                        hull_fit_show = True
                        if hull_fit_show:
                            if  filename_dict.get (dat_fit_hull_filename) == None:
                                filename_dict     [dat_fit_hull_filename]: Dict[str, Any] = {}
                            filename_dict [dat_fit_hull_filename] ['category']  = 'pigment'
                            filename_dict [dat_fit_hull_filename] ['color']     = color_pix
                            filename_dict [dat_fit_hull_filename] ['glyph-pig'] = glyph_pig
                            process_list.append (dat_fit_hull_filename)
                            toclean_list.append (dat_fit_hull_filename)
                            #toclean_list.append (pal_fit_hull_filename)
        elif suffix in ['.pal', '.pig']:
            out_filename = rootname + '.gdat'
            if suffix == '.pig':
                pig2gdat (prog, filename, out_filename)
            else:
                pal2gdat (prog, filename, out_filename)
            if  filename_dict.get (out_filename) == None:
                filename_dict     [out_filename]: Dict[str, Any] = {}
            filename_dict [out_filename] ['category']  = 'pigment'
            filename_dict [out_filename] ['color']     = color_pix
            filename_dict [out_filename] ['glyph-pig'] = glyph_pig
            process_list.append (out_filename)
            toclean_list.append (out_filename)
        else:
            raise SystemExit (f'{prog}: unexpected suffix "{suffix}"')
    # -------------------------------------------------------------------------
    # 3. render
    # -------------------------------------------------------------------------
    if average:
        True
    elif render_dim == 2:
        # -------------------------------------------------------------------------
        # 3.1 render in 2d with gnuplot
        # -------------------------------------------------------------------------
        # rgb is obtained as:
        #   <rgb> <-- python hue2rgb.py <hue>
        # but chroma and luminosity are not well adjusted, so fix it by hand
        color_dict: Dict[str, Any] = {}
        color_dict ['violet'] = {'hue': 294,     'rgb': 0x815da7} # extra-spectral boundary, as PV15
        color_dict ['red']    = {'hue':  24,     'rgb': 0xdc2833} # extra-spectral boundary, as PR108
        color_dict ['yellow'] = {'hue':  90,     'rgb': 0xfee815} # unique yellow, as PY3
        color_dict ['green']  = {'hue':  164.25, 'rgb': 0x9157}   # unique yellow, as PG36
        color_dict ['blue']   = {'hue':  237.53, 'rgb': 0x61a0}   # unique yellow, as PB15:3
        # unique red hue=20.14 is very close to extra_spectral red: merged

        label_dict: Dict[str, Any] = {}
        label_dict ['white']   = {'en': 'white',   'fr': 'blanc'}
        label_dict ['black']   = {'en': 'black',   'fr': 'noir'}
        label_dict ['yellow']  = {'en': 'yellow',  'fr': 'jaune'}
        label_dict ['green']   = {'en': 'green',   'fr': 'vert'}
        label_dict ['cyan']    = {'en': 'cyan',    'fr': 'cyan'}
        label_dict ['blue']    = {'en': 'blue',    'fr': 'bleu'}
        label_dict ['indigo']  = {'en': 'indigo',  'fr': 'indigo'}
        label_dict ['violet']  = {'en': 'violet',  'fr': 'violet'}
        label_dict ['purple']  = {'en': 'purple',  'fr': 'pourpre'}
        label_dict ['magenta'] = {'en': 'magenta', 'fr': 'magenta'}
        label_dict ['red']     = {'en': 'red',     'fr': 'rouge'}
        label_dict ['orange']  = {'en': 'orange',  'fr': 'orange'}
        bs = '\\'
        bb = '{'
        be = '}'

        pdf_list = []
        for mode in ['ab', 'aj', 'bj']:
            plot_filename = main_rootname + '-' + mode + '.plot'
            pdf_filename  = main_rootname + '-' + mode + '.pdf'
            plot = open (plot_filename, 'w')
            sys.stderr.write (f'! file "{plot_filename}" created\n')
            pdf_list.append (pdf_filename)
            toclean_list.append (plot_filename)
            #toclean_list.append (pdf_filename)
            #standalone = 'standalone'
            standalone = '' # for pdf reproductibility, otherwise cannot remove date, see plot2pdf
            plot.write (f"""
set terminal cairolatex pdf color {standalone}
set output "{main_rootname}-{mode}.tex"

xmax = 1.0
set size square
set nokey
set noxtics
set noytics
set parametric
set xrange [-xmax:xmax]
set yrange [-xmax:xmax]
set trange [0:2*pi]
set noborder

# gnuplot hsv2rgb: is slower and less accurate than providing rgb from python
# explanation : hsv is NOT lch or jch : this is another colorspace,
# and there is no support in colorspacious python for hsv
# so using lch for hsv value is incorrect !
h0   =  30. # shift hue, for yellow to be at h=90 deg
cmax =  70. # instead of 100. : push chroma
lmax =  70. # instead of 100. : push luminosity
min(x,y) = (x < y) ? x : y
#my_hsv2rgb(rgb,h,s,v) = hsv2rgb((h-h0)/360.,min(1.,s/cmax),min(1.,v/lmax))
#my_hsv2rgb(rgb,h,s,v) = rgb   # rescue: for tests of hsv2rgb
my_hsv2rgb(rgb) = rgb   # simplified now
""")
            if mode == 'ab':
                # -------------------------------------------------------------
                # ab mode
                # -------------------------------------------------------------
                plot.write ("""
set label '[l]{$a$}' at graph 0.96, 0.52
set label '[l]{$b$}' at graph 0.51, 0.98

set arrow from -xmax,0 to xmax,0 nohead lw 0.5 dt (1,4)
set arrow from 0,-xmax to 0,xmax nohead lw 0.5 dt (1,4)
set arrow from -cos(pi/6),   -sin(pi/6)   to cos(pi/6),   sin(pi/6)   nohead lw 0.5 dt (1,4)
set arrow from -cos(pi/3),   -sin(pi/3)   to cos(pi/3),   sin(pi/3)   nohead lw 0.5 dt (1,4)
set arrow from -cos(2*pi/3), -sin(2*pi/3) to cos(2*pi/3), sin(2*pi/3) nohead lw 0.5 dt (1,4)
set arrow from -cos(5*pi/6), -sin(5*pi/6) to cos(5*pi/6), sin(5*pi/6) nohead lw 0.5 dt (1,4)
set arrow from -xmax,0 to xmax,0 nohead lw 0.5 dt (1,4)

# lines at the boundaries of extra-spectral region and also at unique hues
""")

                for color in color_dict:
                    hue = color_dict [color] ['hue']
                    rgb = color_dict [color] ['rgb']
                    hue_rad = f'2.*pi*{hue}/360.'
                    plot.write (f'set arrow from 0,0 to cos({hue_rad}), sin({hue_rad}) nohead lw 4.0 dt (16,4) lc rgb {rgb}\n')
                yellow  = label_dict ['yellow'] [lang]
                green   = label_dict ['green']  [lang]
                cyan    = label_dict ['cyan']   [lang]
                blue    = label_dict ['blue']   [lang]
                indigo  = label_dict ['indigo'] [lang]
                violet  = label_dict ['violet'] [lang]
                purple  = label_dict ['purple'] [lang]
                magenta = label_dict ['magenta'][lang]
                red     = label_dict ['red']    [lang]
                orange  = label_dict ['orange'] [lang]
                plot.write (f"""
# extended border:
set label '[c]{bb}{bs}textbf{bb}{yellow}{be}{be}'  at 0,    1.05
set label '[c]{bb}{bs}emph{bb}{indigo}{be}{be}'    at 0,   -1.05
set label '[r]{bb}{bs}textbf{bb}{cyan}{be}{be}'    at -sqrt(0.75)-0.01, -0.50
set label '[l]{bb}{bs}textbf{bb}{purple}{be}{be}'  at  sqrt(0.75)+0.01, -0.50
set label '[r]{bb}{bs}emph{bb}{green}{be}{be}'     at -sqrt(0.75)-0.01,  0.50
set label '[l]{bb}{bs}emph{bb}{red}{be}{be}'       at  sqrt(0.75)+0.01,  0.50
set label '[l]{bb}{bs}emph{bb}{magenta}{be}{be}'   at 1.02, 0
set label '[r]{bb}{bs}emph{bb}{blue}{be}{be}'      at -0.5, -sqrt(0.75)-0.01
set label '[l]{bb}{bs}emph{bb}{violet}{be}{be}'    at  0.5, -sqrt(0.75)-0.01
set label '[l]{bb}{bs}emph{bb}{orange}{be}{be}'    at  0.5,  sqrt(0.75)+0.01

plot \\
""")
            else: 
                # -------------------------------------------------------------
                # mode = 'aj' or 'bj' or cut-hue : 'cj'
                # -------------------------------------------------------------
                if cut_hue == 0:
                    if mode == 'aj':
                        horiz_name = 'a'
                    else:
                        horiz_name = 'b'
                else:
                    if mode == 'aj':
                        horiz_name  = 'c_1'
                        if cut_hue < 0: cut_hue = cut_hue + 360
                        cut_hue_rot = f'{cut_hue:.4g}'
                    else:
                        horiz_name="c_2"
                        h_rot = cut_hue+90
                        if h_rot > 360: h_rot = h_rot - 360
                        cut_hue_rot = f'{h_rot:.4g}'

                    plot.write (f"set label '[r]{bb}{bs}small cut $h={cut_hue_rot}${be}' at graph 0.98, 0.99\n")
                plot.write ('set label \'[l]{$%s$}\' at graph 0.96, 0.52\n' % horiz_name)
                plot.write ('set label \'[l]{$j$}\'  at graph 0.53, 0.96\n')
                white = label_dict ['white'] [lang]
                black = label_dict ['black'] [lang]
                plot.write ('set label \'[c]{\\textbf{%s}}\' at 0,  1.05\n' % white)
                plot.write ('set label \'[c]{\\textbf{%s}}\' at 0, -1.05\n' % black)
                plot.write ("""
# horizontal lines
set arrow from -1,0 to 1, 0 nohead lw 1.5 dt (1,4) lc rgb '#000000'
set arrow from -cos(pi/6), 0.5,-0.5 to cos(pi/6), 0.5 nohead lw 1.5 dt (1,4) lc rgb '#000000'
set arrow from -cos(pi/6),-0.5,-0.5 to cos(pi/6),-0.5 nohead lw 1.5 dt (1,4) lc rgb '#000000'

n = 21
do for [i=0:n-1:1] {
  z0n = (i + 0.5)/n
  z0 = 2*z0n - 1
  z1 = z0 - 1./n
  z2 = z0 + 1./n
  g0 = int(256*z0n)
  g  = g0*(1 + 256 + 256*256)
  #print sprintf('gray(%g) = %08x', z0n, g)
  set obj i+1 rect from -1./n, z1 to 1./n, z2 fs solid 1.0 fc rgb g behind
}

plot \\
""")
            # -----------------------------------------------------------------------------
            # .plot : plot body command, loop on gdat files
            # -----------------------------------------------------------------------------
            i_process = 0
            n_process = len(process_list)
            for gdat_filename in process_list:
                i_process += 1
                last_coma = ',' if i_process < n_process else ''
                color_pix = filename_dict [gdat_filename] ['color']  
                category  = filename_dict [gdat_filename] ['category']
                #sys.stderr.write (f'TRACE mode={mode} gdat_filename={gdat_filename} category={category}...\n')
                if category == 'data':
                    pt = 5 if category != 'data' else 7
                    ps = 0.5*scale_pix
                else:
                    pt = 7
                    ps = 2.0*scale_pig
                    fs = int(0.5 + 4.*scale_pig)
                    # convert fs=fontsize to latex 
                    if fs == 8:
                        font = 'normalsize'
                    elif fs < 8:
                        font = 'tiny'
                    else:
                        font = 'Large'

                if mode == 'ab':
                    # -------------------------------------------------------------
                    # ab mode
                    # -------------------------------------------------------------
                    if category == 'pigment':
                        plot.write (f"""'{gdat_filename}' {bs}
  using ($3/100.):($4/100.)  {bs}
  w l lw 4 lc rgb '#000000', {bs}
""")
                    if category in ['pigment', 'data']:
                        if color_pix == 'default':
                            plot.write (f"""'{gdat_filename}' {bs}
  using ($3/100.):($4/100.):(my_hsv2rgb($5))  {bs}
  w p pt {pt} ps {ps} linecolor rgb variable, {bs}
""")
                        else:
                            plot.write (f"""'{gdat_filename}' {bs}
  using ($3/100.):($4/100.)                        {bs}
  w p pt {pt} ps {ps} linecolor rgb '{color_pix}', {bs}
""")
                    else: # category == 'hull':
                        color = color_pix if color_pix != 'default' else '#000000'
                        plot.write (f"""'{gdat_filename}' {bs}
  using ($3/100.):($4/100.)              {bs}
  w lp lw 0.5 dt (1,4) pt 1 ps 0.5 linecolor rgb '{color}', {bs}
""")
                    if category == 'pigment':
                        plot.write (f"""'{gdat_filename}' {bs}
  using ($3/100.):($4/100.):(sprintf('{bb}{bs}{bs}{font} %s{be}', stringcolumn(1))) {bs}
  w labels lc rgb '#ffffff', {bs}
""")

                    plot.write (f"""(cos(t)),(sin(t)) w l lw 1.5 dt (1,4) lc rgb '#000000', {bs}
(0.5*cos(t)),(0.5*sin(t)) w l lw 1.5 dt (1,4) lc rgb '#000000'{last_coma} {bs}
""")
                else:
                    # -------------------------------------------------------------
                    # aj or bj mode
                    # -------------------------------------------------------------
                    if mode == 'aj':
                        hue_angle = f'2*pi*{cut_hue}/360.'
                    else:
                        hue_angle = f'2*pi*({cut_hue}+90)/360.'
                    horiz_c = f'(cos({hue_angle})*$3+sin({hue_angle})*$4)/100.'
                    if category == 'pigment':
                        plot.write (f"""'{gdat_filename}' {bs}
  using ({horiz_c}):(2*$2/100.-1) {bs}
  w l lw 4 lc rgb '#000000',      {bs}
""")
                    if category in ['pigment', 'data']:
                        if color_pix == 'default':
                            plot.write (f"""'{gdat_filename}' {bs}
  using ({horiz_c}):(2*$2/100. - 1.):(my_hsv2rgb($5)) {bs}
  w p pt {pt} ps {ps} linecolor rgb variable, {bs}
""")
                        else:
                            plot.write (f"""'{gdat_filename}' {bs}
  using ({horiz_c}):(2*$2/100. - 1.) {bs}
  w p pt {pt} ps {ps} linecolor rgb '{color_pix}', {bs}
""")
                    else: # category == 'hull':
                        color = color_pix if color_pix != 'default' else '#000000'
                        plot.write (f"""'{gdat_filename}' {bs}
  using ({horiz_c}):(2*$2/100. - 1)      {bs}
  w lp lw 0.5 dt (1,4) pt 1 ps 0.5 linecolor rgb '{color}', {bs}
""")
                    if category == 'pigment':
                        plot.write (f"""'{gdat_filename}' {bs}
  using ({horiz_c}):(2*$2/100. - 1.):(sprintf('{bb}{bs}{bs}{font} %s{be}', stringcolumn(1))) {bs}
  w labels lc rgb '#ffffff', {bs}
""")
                    plot.write (f"(cos(t)),(sin(t)) w l lw 1.5 dt (1,4) lc rgb '#000000'{last_coma} {bs}\n")

            plot.close()
            # run plot2pdf
            command = f'{PLOT2PDF} {plot_filename}'
            if verbose:
                sys.stderr.write (f'! {command}\n')
            result = subprocess.run (command, shell=True, check=True)
            if result.returncode != 0:
                raise SystemExit (f'{prog}: plo2pdf command failed (status={result.returncode})')
            toclean_list.append ('gnuplot.cfg')

        # end for mode in ['ab', 'aj', 'bj']
        if not batch:
            command = f'evince'
            for pdf_filename in pdf_list:
                command = command + ' ' + pdf_filename
            command = command + ' &'
            if verbose:
                sys.stderr.write (f'! {command}\n')
            result = subprocess.run (command, shell=True, check=True)
            if result.returncode != 0:
                raise SystemExit (f'{prog}: evince command failed (status={result.returncode})')

    else:
        # -------------------------------------------------------------------------
        # 3.2 render in 3d with paraview
        # -------------------------------------------------------------------------
        sys.stderr.write (f'TRACE glyph={glyph_pig} for global\n')
        opt = {
            'scale-pig'    : scale_pig,
            'scale-pix'    : scale_pix,
            'glyph-pig'    : glyph_pig,
            'opacity'      : opacity,
            'circle-resol' : circle_resol,
            'vtk'          : vtk,
            'stereo'       : stereo,
        }
        process_pair_list = []
        for gdat_filename in process_list:
            category  = filename_dict [gdat_filename] ['category']
            color_pix = filename_dict [gdat_filename] ['color']  
            glyph_pig = filename_dict [gdat_filename] ['glyph-pig']
            if color_pix == 'default':
                color_pix = '#000000'
            sys.stderr.write (f'TRACE glyph={glyph_pig} for {gdat_filename}\n')
            process_pair_list.append ([gdat_filename, category, color_pix, glyph_pig])
        py_filename = main_rootname + '.py'
        py = open (py_filename, 'w')
        sys.stderr.write (f'! file "{py_filename}" created\n')
        toclean_list.append (py_filename)
        py.write (f"""#!/usr/bin/env paraview --script=
from paraview.simple  import *
from paraview_palette import *  # load palette specific functions
process_pair_list = {process_pair_list}
opt = {opt}
paraview_palette (process_pair_list, opt)
""")
        python_path = libdir
        resolution = [835, 763] # default resolution
        if view_rootname != '':
          python_path = python_path + ':' + tmpdir
          py.write (f"""
from {view_rootname} import *
palette_view (resolution, camera_position, camera_focal_point, camera_view_up, camera_parallel_scale)
""")
        if batch:
          py.write (f"""
palette_save ({resolution}, '{main_rootname}-abj.jpg')
""")
        py.close()
        option = '--stereo'; # always available: activated interactively or by "option" dict 
        if not batch:
            command = f'LANG=C PYTHONPATH={python_path} paraview {option} --script={py_filename}'
        else:
            command = f'LANG=C PYTHONPATH={python_path} pvbatch {option} {py_filename}'
        if verbose:
            sys.stderr.write (f'! {command}\n')
        result = subprocess.run (command, shell=True, check=True)
        if result.returncode != 0:
            raise SystemExit (f'{prog}: evince command failed (status={result.returncode})')
    # -------------------------------------------------------------------------
    # 4. clean
    # -------------------------------------------------------------------------
    if clean and len(toclean_list) != 0:
        command = 'rm -f'
        for x in toclean_list:
            command = command + ' ' + x
        if verbose:
            sys.stderr.write (f'! {command}\n')
        result = subprocess.run (command, shell=True, check=True)
        if result.returncode != 0:
            raise SystemExit (f'{prog}: clean command failed (status={result.returncode})')
    

# -------------------------------------------------------------------------
# 5. main call
# -------------------------------------------------------------------------
#palette (sys.argv, libdir)
