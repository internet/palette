# input:
#   hue in degrees  [0:360]
# output:
#   rgb in hexa, as three [0:255] components
# usage
#   python thisfile.py <hue> [<chroma> [luminosity]]
# example:
#   python thisfile.py 20
# note:
#   used by mk_palette.sh for extra-spectral bounds
#   and unique colors positions
#
import sys
from math import *
from colorspacious.conversion import cspace_convert

#print('argv=',sys.argv)
if len(sys.argv) == 1:
   sys.stderr.write ('usage: python thisfile.py <hue> [<chroma> [luminosity]]\n');
   exit(1)

if len(sys.argv) > 1:
  hue = float(sys.argv[1])

cmax = 78.
if len(sys.argv) > 2:
  cmax = float(sys.argv[2])
vmax = cmax
if len(sys.argv) > 3:
  vmax = float(sys.argv[3])

jch = [vmax, cmax, hue]
rgb = cspace_convert(jch, "JCh", "sRGB255")
for i in [0,1,2]:
  rgb[i] = int(round(min(255., max(0., rgb[i]))))
#print(f'hue={hue}, jch: jch{jch} -> rgb{rgb}')
rgb_value = int(rgb[0]*65536 + rgb[1]*256 + rgb[2])
print (f'0x{rgb_value:06x}')

