# input:
#   jch : ciecam in polar (degrees)
# output:
#   lch : cielab in polar (degrees)
# usage
#   python thisfile.py l c h
# example:
#   python thisfile.py 20 19 180
#
import sys
from math import *
from colorspacious.conversion import cspace_convert
from libpalette import *

#print('argv=',sys.argv)
if len(sys.argv) <= 3:
   sys.stderr.write ('usage: python thisfile.py <j> <c> <h>\n');
   exit(1)

l = float(sys.argv[1].rstrip(','))
c = float(sys.argv[2].rstrip(','))
h = float(sys.argv[3].rstrip(','))

lch = [l, c, h]
lab = xch2xab (lch);
rgb3 = cspace_convert(lab, "CIELab", "sRGB255")
rgb = int(rgb3[0]*65536 + rgb3[1]*256 + rgb3[2])
print (f'#{rgb:06x}')
