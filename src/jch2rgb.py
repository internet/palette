# input:
#   jch : ciecam in polar (degrees)
# output:
#   rgb
# usage
#   python thisfile.py l c h
# example:
#   python thisfile.py 20 19 180
#
# note: jch or lab to rgb is buggy in both colorspacious and colormath, eg:
#  python jch2rgb.py  93.975031850498993   8.689156786829367 331.627468059957010
# because rgb components could become greater than 1 or 255: should be bounded
#
import sys
from math import *
from colorspacious.conversion import cspace_convert
from colormath.color_objects import LabColor, XYZColor, sRGBColor
from colormath.color_conversions import convert_color
from libpalette import *

#print('argv=',sys.argv)
if len(sys.argv) <= 3:
   sys.stderr.write ('usage: python thisfile.py <j> <c> <h>\n');
   exit(1)

use_colorspacious = True

j = float(sys.argv[1].rstrip(','))
c = float(sys.argv[2].rstrip(','))
h = float(sys.argv[3].rstrip(','))

jch = [j, c, h]
if use_colorspacious:
    # colorspacious 
    rgb255 = cspace_convert(jch, "JCh", "sRGB255")
#   print (f'rgb255 = {rgb255}')
    for i in range(0,3):
        rgb255[i] = max(0, min(255, int(0.5+rgb255[i])))
#   print (f'rgb255 = {rgb255}')
    rgb = int(rgb255[0]*65536 + rgb255[1]*256 + rgb255[2])
    print (f'#{rgb:06x}')
else:
    # colormath 
    lab = cspace_convert (jch, "JCh", "CIELab")
#   print (f'lab = {lab}')
    lab_obj = LabColor (lab[0], lab[1], lab[2])
    xyz_obj = convert_color (lab_obj, XYZColor)
    rgb_obj = convert_color(xyz_obj, sRGBColor)
    rgb_tuple = rgb_obj.get_value_tuple()
#   print (f'rgb_tuple = {rgb_tuple}')
#   rgb_hex = rgb_obj.get_rgb_hex()
#   print (f'rgb_hex = {rgb_hex}')
#   rgb256 = rgb_obj.get_upscaled_value_tuple()
#   print (f'rgb256 = {rgb256}')
#   rgb1 = [0,0,0]
#   for i in range(0,3):
#       rgb1[i] = max(0, min(1, rgb_tuple[i]))
#   print (f'rgb1= {rgb1}')
    # rgb_tuple could go beyond [0:1]...
    rgb255 = [0, 0, 0]
    for i in range(0,3):
        rgb255[i] = max(0, min(255, int(0.5+255*rgb_tuple[i])))
#   print (f'rgb255 = {rgb255}')
    rgb = int(rgb255[0]*256*256 + rgb255[1]*256 + rgb255[2])
    print (f'#{rgb:06x}')
