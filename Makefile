SUBDIRS = 		\
	src		\
	doc		\

DISTFILES = 		\
	Makefile 	\
	README.rst	\
	TODO		\

default: all

all install clean::
	@for d in $(SUBDIRS); do \
	  cd $$d && $(MAKE) $@ && cd ..; \
	  if test $$? -ne 0; then exit 1; fi; \
	done

-include $(CVSMKROOT)/git.mk
